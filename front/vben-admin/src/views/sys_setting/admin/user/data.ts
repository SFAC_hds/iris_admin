import {VxeGridPropTypes} from "vxe-table/types/grid";

export const columns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: false, width: 60,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'org.code', title: '组织编码', sortable: false, visible: false, width: 100,},
  {field: 'org.name', title: '所属组织', sortable: false, width: 120,},
  {field: 'username', title: '账号', sortable: false, width: 130,},
  {field: 'name', title: '姓名', sortable: false, width: 100,},
  {field: 'roles', title: '角色', sortable: false, cellRender: {name: 'AdminUserRoleList'}, minWidth: 200,},
  {field: 'gender', title: '性别', visible: false, sortable: false, width: 90,},
  {field: 'enable', title: '启用', sortable: false, width: 50,},
  {field: 'age', title: '年龄', visible: false, sortable: false, width: 80,},
  {field: 'phone', title: '电话', sortable: false, width: 140,},
  {field: 'email', title: '电子邮件', visible: false, sortable: false, width: 90,},
  {field: 'userface', title: '头像', visible: false, sortable: false, width: 90,},
  {field: 'memo', title: '备注', visible: false, sortable: false, width: 90,},
  {field: 'createdTime', title: '创建时间', visible: true, sortable: false, width: 140,},
  {field: 'updatedTime', title: '更新时间', visible: false, sortable: false,},
];