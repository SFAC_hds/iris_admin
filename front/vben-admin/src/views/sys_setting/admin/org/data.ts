import {VxeGridPropTypes} from "vxe-table/types/grid";

export const columns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: false, width: 60,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'code', title: '编码', sortable: true, width: 120,},
  {field: 'name', title: '名称', sortable: false, width: 280,},
  {field: 'enable', title: '启用', sortable: false, width: 50,},
  {field: 'area', title: '区域', visible: true, sortable: false, width: 280, cellRender: {name: 'Area'},},
  {field: 'person', title: '联系人', sortable: false, width: 120,},
  {field: 'phone', title: '电话', sortable: false, width: 160,},
  {field: 'officeAddress', title: '办公地址', sortable: false, visible: false, width: 90,},
  {field: 'registerAddress', title: '注册地址', sortable: false, visible: false, width: 90,},
  {field: 'memo', title: '备注', visible: false, sortable: false, width: 100,},
  {field: 'createdTime', title: '创建时间', visible: false, sortable: false,},
  {field: 'updatedTime', title: '更新时间', visible: false, sortable: false,},
];