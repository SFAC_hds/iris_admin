import {VxeGridPropTypes} from "vxe-table/types/grid";

export const columns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: false, width: 60,},
  {field: 'id', title: 'ID', width: 140, visible: false,},
  {field: 'name', title: '角色名称', sortable: false, minWidth: 150,},
  {field: 'memo', title: '备注', sortable: false, minWidth: 150,},
  {field: 'createdTime', title: '创建时间', visible: true, sortable: false,},
  {field: 'updatedTime', title: '更新时间', visible: false, sortable: false,},
  // {title: '操作', sortable: false, slots: {default: 'operate'}},
];

export const menuColumns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: true, width: 130,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'meta.icon', title: '图标', sortable: false, cellRender: {name: 'MenuIcon'}, width: 100,},
  {field: 'meta', title: '菜单名', sortable: false, cellRender: {name: 'MenuTitle'}, width: 140,},
  {field: 'name', title: '内部名称', visible: false, sortable: false,},
  {field: 'path', title: '路径', visible: false, sortable: false,},
  {field: 'component', title: '组件', visible: false, sortable: false,},
  {field: 'redirect', title: '重定向', visible: false, sortable: false,},
  {field: 'parentId', title: '父ID', visible: false,sortable: false, width: 60,},
  {field: 'buttonList', title: '按钮', visible: true, cellRender: {name: 'MenuButtonListOfRole'}, minWidth: 320, },
];

export const policyColumns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: false, width: 60,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'groupName', title: '接口组名', visible: true, sortable: false, type: 'html', width: 120, },
  {field: 'name', title: '接口名', visible: true, sortable: false, type: 'html', width: 180, },
  {field: 'isSystem', title: '系统', visible: false, sortable: false, width: 70,},
  {field: 'memo', title: '接口说明', visible: false, sortable: false, width: 120,},

  {field: 'v1', title: '租户', visible: false, sortable: false, width: 90},
  {field: 'v2', title: '接口', visible: true, sortable: false, type: 'html', width: 210,},
  {field: 'v3', title: 'Methods', visible: true, sortable: false, type: 'html', width: 90,},
  {field: 'v4', title: 'Suf', visible: false, sortable: false, width:50,},
  {field: 'v5', title: '额外的', visible: false, sortable: false, width: 60,},
];