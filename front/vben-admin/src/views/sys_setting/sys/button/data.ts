import {VxeGridPropTypes} from "vxe-table/types/grid";

export const columns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: false, width: 60,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'group', title: '按钮组', visible: false, sortable: false,},
  {field: 'name', title: '按钮名称', visible: true, sortable: false, width: 150,},
  {field: 'icon', title: '图标', visible: true, sortable: false, width: 70,},
  {field: 'place', title: '按钮位置', visible: true, sortable: false, width: 90,},
  {field: 'func', title: '方法', visible: true, sortable: false, width: 130,},
  {field: 'disable', title: '默认禁用', visible: true, sortable: false, width: 130,},
  {field: 'tip', title: '是否提示', visible: true, sortable: false, width: 80,},
  {field: 'tipContent', title: '提示内容', visible: true, sortable: false, width: 150,},
  {field: 'memo', title: '备注', visible: false, sortable: false, width: 120,},
  {field: 'creatorId', title: '创建人', visible: false, sortable: false,},
  {field: 'createdTime', title: '创建时间', visible: false, sortable: false,},
  {field: 'updatedTime', title: '更新时间', visible: false, sortable: false,},
];

//
export interface NameValue {
  label: string,
  value: string,
}

export const nameOptions = [
  {value: 'create', label: '新增',},
  {value: 'edit', label: '编辑',},
  {value: 'batchDelete', label: '删除',},
  {value: 'copy', label: '复制行',},
  {value: 'grant', label: '授权',},
];

export const placeOptions = [
  {value: 0, label: '表格上方-左边',},
  {value: 1, label: '表格上方-右边',},
  {value: 2, label: '表格内-操作',},
  {value: 3, label: '其他',},
];

export const disabledOptions = [
  {value: 0, label: '默认可交互',},
  {value: 1, label: '默认不可交互-没数量',},
  {value: 2, label: '默认不可交互-有数量',},
];