import {VxeGridPropTypes} from "vxe-table/types/grid";

export const columns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  // {title: '操作', sortable: false, slots: {default: 'operate'}, width: 180, align: 'center',},
  {type: 'seq', title: '序号', treeNode: true, width: 110,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'meta.icon', title: '图标', sortable: false, cellRender: {name: 'MenuIcon'}, width: 60,},
  {field: 'meta', title: '名称', sortable: false, cellRender: {name: 'MenuTitle'}, width: 150,},
  {field: 'name', title: '内部名称', visible: false, sortable: false, width: 90,},
  {field: 'path', title: '路径', sortable: false, width: 100,},
  {field: 'component', title: '组件', sortable: false, width: 220,},
  {field: 'redirect', title: '重定向', sortable: false, width: 180,},
  {field: 'parentId', title: '父ID', visible: false, sortable: false, width: 60,},
  {field: 'sort', title: '顺序', sortable: false, width: 60,},
  {field: 'buttonList', title: '按钮', sortable: false, cellRender: {name: 'MenuButtonList'}, minWidth: 300,},
  {field: 'metaId', title: 'MetaID', visible: false, sortable: false, width: 60,},
  // {field: 'meta', title: '元数据', visible: false, sortable: false,},
  {field: 'createdTime', title: '创建时间', visible: false, sortable: false, width: 110,},
  {field: 'updatedTime', title: '更新时间', visible: false, sortable: false, width: 110,},
];