import {VxeGridPropTypes} from "vxe-table/types/grid";

export const columns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: false, width: 60,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'groupName', title: '接口组名', visible: true, sortable: false,},
  {field: 'name', title: '接口名', visible: true, sortable: false,},
  {field: 'memo', title: '接口说明', visible: false, sortable: false,},
  {field: 'v1', title: '租户', visible: true, sortable: false, width: 95,},
  {field: 'v2', title: '接口', visible: true, sortable: false, minWidth: 130,},
  {field: 'v3', title: 'Methods', visible: true, sortable: false, width: 85,},
  {field: 'v4', title: 'Suf', visible: true, sortable: false, width:50,},
  {field: 'v5', title: '额外的', visible: false, sortable: false, width: 60,},
  {field: 'logable', title: '日志', visible: true, sortable: false, width: 60,},
  {field: 'isSystem', title: '系统', visible: false, sortable: false, width: 60,},
];

export const printStyle = `
.title {
  text-align: center;
}
.my-list-row {
  display: inline-block;
  width: 100%;
}
.my-list-col {
  float: left;
  width: 33.33%;
  height: 28px;
  line-height: 28px;
}
.my-top,
.my-bottom {
  font-size: 12px;
}
.my-top {
  margin-bottom: 5px;
}
.my-bottom {
  margin-top: 30px;
  text-align: right;
}
`
// 打印顶部内容模板
export const topHtml = `
<h1 class="title">出货单据</h1>
<div class="my-top">
  <div class="my-list-row">
    <div class="my-list-col">商品名称：vxe-table</div>
    <div class="my-list-col">发货单号：X2665847132654</div>
    <div class="my-list-col">发货日期：2020-09-20</div>
  </div>
  <div class="my-list-row">
    <div class="my-list-col">收货姓名：小徐</div>
    <div class="my-list-col">收货地址：火星第七区18号001</div>
    <div class="my-list-col">联系电话：10086</div>
  </div>
</div>
`

// 打印底部内容模板
export const bottomHtml = `
<div class="my-bottom">
  <div class="my-list-row">
    <div class="my-list-col"></div>
    <div class="my-list-col">创建人：小徐</div>
    <div class="my-list-col">创建日期：2020-09-20</div>
  </div>
</div>
`
