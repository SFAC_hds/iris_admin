import {VxeGridPropTypes} from "vxe-table/types/grid";

export const columns: VxeGridPropTypes.Columns = [
  {type: 'checkbox', width: 40,},
  {type: 'seq', title: '序号', treeNode: false, width: 60,},
  {field: 'id', title: 'ID', width: 60, visible: false,},
  {field: 'creator.name', title: '用户', sortable: false, minWidth: 80,},
  {field: 'groupName', title: '模块', sortable: false, minWidth: 90,},
  {field: 'name', title: '子模块', sortable: false, minWidth: 90,},
  {field: 'duration', title: '耗时', titlePrefix: {content: '毫秒:1/1000s'}, sortable: true, width: 75,},
  {field: 'path', title: 'url', sortable: false, minWidth: 80,},
  {field: 'method', title: 'method', sortable: false, width: 65,},
  {field: 'params', title: 'params', sortable: false, minWidth: 90,},
  {field: 'data', title: 'data', sortable: false, minWidth: 90,},
  {field: 'respStatusCode', title: '响应码', sortable: false, minWidth: 70,},
  {field: 'ipAddr', title: 'IP', sortable: false, width: 90,},
  {field: 'city', title: '省市', sortable: false, width: 90,},
  {field: 'os', title: '操作系统', sortable: false, width: 65,},
  {field: 'userAgent', title: '浏览器', sortable: false, width: 65,},
  {field: 'createdTime', title: '操作时间', visible: true, sortable: true, width: 120,},
  // {field: 'updatedTime', title: '更新时间', visible: false, sortable: false,},
  {title: '操作', sortable: false, width: 60, slots: {default: 'operate'}},
];