import VXETable from 'vxe-table'
import { useI18n } from '/@/hooks/web/useI18n';
import { Icon } from '/@/components/Icon';
import {getName} from '/@/utils/data/area';
import {Image, Tag,
  Checkbox, CheckboxGroup,
  Divider,
} from 'ant-design-vue'
// import {useGo} from '/@/hooks/web/usePage';

// const go = useGo();

VXETable.renderer.add('MenuTitle', {
  // 默认显示模板
  renderDefault (renderOpts, params) {
    const { t } = useI18n();
    let { row } = params
    if (row.meta) {
      return t(row.meta.title)
    }
    return [];
  },
  // 导出模板，例如导出插槽中自定义的内容
  exportMethod (params) {
    const { row, column } = params
    return row.bannerUrl;
  }
})

VXETable.renderer.add('MenuIcon', {
  renderDefault (renderOpts, params) {
    let { row } = params
    if (row.meta.icon) {
      return <Icon icon={row.meta.icon}/>
    }
    return [];
  },
  // 导出模板，例如导出插槽中自定义的内容
  exportMethod (params) {
    const { row, column } = params
    return row.bannerUrl;
  }
})
// MenuButtonList
VXETable.renderer.add('MenuButtonList', {
  renderDefault (renderOpts, params) {
    let { row } = params
    let result = [] as any[]
    if (row.buttonList) {
      row.buttonList.forEach(e => {
        // result.push(<Tag style="margin: 2px 2px" color="default">{e.name}</Tag>);
        result.push(<a title={e.memo} style="color:grey;cursor:default;">{e.name}<Divider type="vertical"/></a>);
      })
    }
    return result;
  },
  // 导出模板，例如导出插槽中自定义的内容
  exportMethod (params) {
    const { row, column } = params
    return row.bannerUrl;
  }
})

VXETable.renderer.add('MenuButtonListOfRole', {
  renderDefault (renderOpts, params) {
    let { row, $table } = params
    let one = $table.getCheckboxRecords(true).filter(e => e.id === row.id)
    let disabled = !(one.length > 0)
    const options = row.buttonList && row.buttonList.map(e => {
      return {'label': e.name, 'value': e.id}
    }) || []
    return <CheckboxGroup v-model:value={row.ownedBidList} options={options} disabled={disabled}></CheckboxGroup>
  },
})

VXETable.renderer.add('AdminUserRoleList', {
  renderDefault: function (renderOpts, params) {
      let {row,} = params
      let result: any[] = []
      row.roles.forEach((e: any) => {
        result.push(<Tag color="orange">{e.name}</Tag>)
      })
      return result;
  },
})

VXETable.renderer.add('Area', {
  renderDefault: function (renderOpts, params) {
      let {row,} = params
      if (row.area) {
        let codeList:any[] = row.area.split(',')
        let nameList = codeList.map(e => getName(e))
        return nameList.join(' / ');
      }
      return '';
  },
})
