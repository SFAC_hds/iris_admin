export default {
  default: 'System Setting',
  admin: {
    admin: 'Organization Framework',
    user: 'User',
    role: 'Role',
    org: 'Organization',
  },
  sys: {
    sys: 'System',
    monitor: 'Monitor',
    log: 'Log',
    menu: 'Menu',
    policy: 'Policy',
    button: 'Button',
  }
};