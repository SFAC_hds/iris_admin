export default {
  default: '系统设置',
  admin: {
    admin: '组织架构',
    user: '用户',
    role: '角色',
    org: '多组织',
  },
  sys: {
    sys: '系统',
    monitor: '系统监控',
    log: '操作日志',
    menu: '菜单',
    policy: '接口策略',
    button: '按钮',
  }
};