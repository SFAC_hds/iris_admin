import { defHttp } from '/@/utils/http/axios';
import { LoginParams, LoginResultModel, GetUserInfoModel } from './model/userModel';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  Login = '/user/login',
  Logout = '/user/logout',
  GetUserInfo = '/user/getUserInfo',
  ResetPassword = '/user/resetPassword',
  TestRetry = '/user/testRetry',

  //
  Crud = '/user',
  List = '/user/list',
  Table = '/user/table',
}

/**
 * @description: user login api
 */
export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: getUserInfo
 */
export function getUserInfo() {
  return defHttp.get<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}

export function doLogout() {
  return defHttp.get({ url: Api.Logout });
}

export function testRetry() {
  return defHttp.get(
    { url: Api.TestRetry },
    {
      retryRequest: {
        isOpenRetry: true,
        count: 5,
        waitTime: 1000,
      },
    },
  );
}


//
export const getTableData = (params:any) => {
  return defHttp.get<GetUserInfoModel>({ url: Api.Table, params: params });
};

export const getListData = () =>
  defHttp.get<GetUserInfoModel>({ url: Api.List });

export const create = (params: any) =>
  defHttp.post({url: Api.Crud, params});

export const update = (params: any) =>
  defHttp.put({url: Api.Crud, params});

export const deletes = (params: number[]) =>
  defHttp.delete({url: Api.Crud, data: params});

  export const resetPassword = (params: any) => defHttp.post({url: Api.ResetPassword, data: params}, {errorMessageMode: 'modal'});
