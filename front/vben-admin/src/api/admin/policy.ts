import { defHttp } from '/@/utils/http/axios';
import { AdminPolicyApiModel } from './model/policyModel';

enum Api {
  Crud = '/policy',
  Table = '/policy/table',
  ReloadPolicy = '/policy/reloadPolicy',
  List = '/policy/list',
  Log = '/policy/log',
}

export const getTableData = (params:any) => defHttp.get<AdminPolicyApiModel>({ url: Api.Table, params: params });

export const getListData = () => defHttp.get<AdminPolicyApiModel>({ url: Api.List });

export const deletes = (params: number[]) => defHttp.delete({url: Api.Crud, data: params});

export const reloadPolicy = () => defHttp.get<any>({ url: Api.ReloadPolicy });

export const log = (params:any) => defHttp.put<any>({ url: Api.Log, params });