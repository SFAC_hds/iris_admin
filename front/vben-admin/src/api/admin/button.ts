import { defHttp } from '/@/utils/http/axios';
import { AdminButtonApiModel } from './model/buttonModel';

enum Api {
  Crud = '/button',
  Generate = '/button/generate',
  List = '/button/list',
  CodeList = '/button/codeList',
}

export const getListData = () => defHttp.get<AdminButtonApiModel>({ url: Api.List });

export const create = (params: any) => defHttp.post({url: Api.Crud, params});

export const update = (params: any) => defHttp.put({url: Api.Crud, params});

export const deletes = (params: number[]) => defHttp.delete({url: Api.Crud, data: params});

export const generate = () => defHttp.post({url: Api.Generate});

export const getPermCode = () => {
  // return defHttp.get<string[]>({ url: Api.CodeList });
}