import { defHttp } from '/@/utils/http/axios';
import { AdminMenuApiModel } from './model/menuModel';

enum Api {
  Crud = '/menu',
  Table = '/menu/table',
  List = '/menu/list',
}

/**
 * @description: Get user menu based on id
 */

export const getTableData = (params:any) => defHttp.get<AdminMenuApiModel>({ url: Api.Table, params: params });

export const getListData = (params: any) => defHttp.get<AdminMenuApiModel>({ url: Api.List, params });

export const create = (params: any) => defHttp.post({url: Api.Crud, params});

export const update = (params: any) => defHttp.put({url: Api.Crud, params});

export const deletes = (params: number[]) => defHttp.delete({url: Api.Crud, data: params});
