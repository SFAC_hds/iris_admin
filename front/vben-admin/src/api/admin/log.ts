import { defHttp } from '/@/utils/http/axios';
import { AdminLogApiModel } from './model/logModel';

enum Api {
  Crud = '/log',
  Table = '/log/table',
}

export const getTableData = (params:any) => {
  return defHttp.get<AdminLogApiModel>({ url: Api.Table, params: params });
};
export const deletes = (params: number[]) => defHttp.delete({url: Api.Crud, data: params});

