import { defHttp } from '/@/utils/http/axios';
import { AdminOrgApiModel } from './model/orgModel';

enum Api {
  Crud = '/org',
  Table = '/org/table',
  List = '/org/list',
}

export const getTableData = (params:any) => defHttp.get<AdminOrgApiModel>({ url: Api.Table, params: params });

export const getListData = () => defHttp.get<AdminOrgApiModel>({ url: Api.List });

export const create = (params: any) => defHttp.post({url: Api.Crud, params});

export const update = (params: any) => defHttp.put({url: Api.Crud, params});

export const deletes = (params: number[]) => defHttp.delete({url: Api.Crud, data: params});

