import { defHttp } from '/@/utils/http/axios';
import { AdminRoleApiModel } from './model/roleModel';

enum Api {
  Crud = '/role',
  Table = '/role/table',
  List = '/role/list',
  Grant = '/role/grant',
  Owned = '/role/grant/owned',
}

export const getTableData = (params:any) => {
  return defHttp.get<AdminRoleApiModel>({ url: Api.Table, params: params });
};

export const getListData = () =>
  defHttp.get<AdminRoleApiModel>({ url: Api.List });

export const create = (params: any) =>
  defHttp.post({url: Api.Crud, params});

export const update = (params: any) =>
  defHttp.put({url: Api.Crud, params});

export const deletes = (params: number[]) =>
  defHttp.delete({url: Api.Crud, data: params});

export const grant = (params: any) => defHttp.post({url: Api.Grant, data: params});

export const owned = (params: any) => defHttp.get({url: Api.Owned, params: params});

