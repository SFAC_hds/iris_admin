export interface ButtonItem {
  id: number,
  group: string;
  name: string;
  icon: string;
  memo: string;
  sort: number;
  children?: ButtonItem[];
}

/**
 * @description: Get menu return value
 */
export type AdminButtonApiModel = ButtonItem[];
