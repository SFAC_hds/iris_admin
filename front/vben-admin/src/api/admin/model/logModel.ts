export interface LogItem {
  id: number,
  name: string;
  
}

/**
 * @description: Get menu return value
 */
export type AdminLogApiModel = LogItem[];
