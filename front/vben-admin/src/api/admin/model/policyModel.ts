
export interface PolicyItem {
  id: number,

  groupName: string,
	name:      string,
	memo:      string,
	isSystem:  boolean,

  v1: string,
  v2: string,
  v3: string,
  v4: string,
  v5: string,
}

/**
 * @description: Get menu return value
 */
export type AdminPolicyApiModel = PolicyItem[];
