export interface Item {
  id: number,
  code: string;
  name: string;
  area: string
  enable: number,
  memo: string;
}

export type AdminOrgApiModel = Item[];
