export interface RoleItem {
  id: number,
  name: string;
  memo: string;
}

/**
 * @description: Get menu return value
 */
export type AdminRoleApiModel = RoleItem[];
