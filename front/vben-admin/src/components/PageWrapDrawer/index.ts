import { withInstall } from '/@/utils';
import Index from './src/index.vue';

export const PageWrapDrawer = withInstall(Index);
