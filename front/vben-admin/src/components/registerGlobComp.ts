import type { App } from 'vue';
import { Button } from './Button';
import { Input, Layout } from 'ant-design-vue';
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'
import VXETablePluginExportXLSX from 'vxe-table-plugin-export-xlsx'


export function registerGlobComp(app: App) {
  VXETable.use(VXETablePluginExportXLSX)
  app.use(Input).use(Button).use(Layout).use(VXETable);
}
