# Iris Admin
Iris Admin 是一个Go语言开发的大中型平台系统。
<div align="center">
    <a href='https://gitee.com/yhm_my/go-iris'><img src='https://gitee.com/yhm_my/go-iris/widgets/widget_1.svg' alt='golang iris web'></img></a>
    <p>
        <h1>Iris Admin V1.0</h1>
    </p>
    <p align="center">
        <a href="https://github.com/kataras/iris" target="_blank">
	        <img src="https://img.shields.io/badge/iris-v12.2.3-green" alt="iris">
	    </a>
	    <a href="https://v3.vuejs.org/" target="_blank">
	        <img src="https://img.shields.io/badge/vue.js-vue3.x-green" alt="vue">
	    </a>
	    <a href="https://www.antdv.com/components/overview" target="_blank">
	        <img src="https://img.shields.io/badge/Ant%20Design%20Vue-%3E%3D3.2-green" alt="Ant Design Vue">
	    </a>
		<a href="https://www.tslang.cn/" target="_blank">
	        <img src="https://img.shields.io/badge/typescript-%3E4.0.0-blue" alt="typescript">
	    </a>
		<a href="https://vitejs.dev/" target="_blank">
		    <img src="https://img.shields.io/badge/vite-%3E2.0.0-yellow" alt="vite">
		</a>
	</p>
</div>

#### [去看bilibili的Iris教程](https://www.bilibili.com/video/BV1y24y1k7Fd/?spm_id_from=333.999.0.0&vd_source=d4d6592b2817c23818e6e8de715fdbe1)

## 平台介绍
* 平台采用Golang Iris 12.2 + Vue3 + Ant Design Vue开发。
* 前端采用Vben Admin框架。

## 特点
* 会话机制：采用业界最流行的token认证；
* 权限认证：采用强大、简洁、跨平台的casbin进行权限认证，更灵活、精细的资源权限控制；
* 精细的权限粒度：菜单、按钮动态控制；
* UI及交互：界面漂亮、交互很棒、暗黑模式、系统锁屏、自由切换tab时正在编辑中的数据可缓存，且可动态配置等等；

## 内置功能
* 用户：用户是系统操作者，该功能主要完成系统用户配置。
* 部门或职位：本系统不实现！考虑到部门主要用于OA中，而OA有很成熟完善的三方平台,如：钉钉...等。所以只需要做系统对接而不需要自己实现了。
* 菜单：配置系统菜单，按钮权限等。
* 角色：管理系统角色、及角色拥有的菜单、接口权限等。
* 接口策略：对接口权限操作进行管理。
* 操作日志：对需要关心的操作日志进行埋点记录，用于监控、分析。
* 服务监控：监视当前系统CPU、内存、goroutine、堆栈等相关信息并推送给前端（6s，可配置）。
* 权限粒度：（菜单+按钮）细粒度权限动态控制。

## 演示地址DEMO
<a target="_blank">http://47.109.26.45:10087/#/dashboard/analysis</a>
<br/>
账号：demo
密码：demo123

## 视频教程
请耐心等一等

## 文档
<a href="https://gitee.com/yhm_my/iris_admin/wikis" target="_blank">请移步 wikis 查看</a>

## 演示图
<table>
    <tr>
        <td><img src="https://gitee.com/yhm_my/iris_admin/raw/master/img/demo1.png"/></td>
        <td><img src="https://gitee.com/yhm_my/iris_admin/raw/master/img/demo2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/yhm_my/iris_admin/raw/master/img/demo3.png"/></td>
        <td><img src="https://gitee.com/yhm_my/iris_admin/raw/master/img/demo4.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/yhm_my/iris_admin/raw/master/img/demo5.png"/></td>
        <td><img src="https://gitee.com/yhm_my/iris_admin/raw/master/img/demo6.png"/></td>
    </tr>
</table>

## 感谢(排名不分先后)
> Iris框架 [https://github.com/kataras/iris](https://github.com/kataras/iris)
>
> Vben-Admin [https://github.com/vbenjs/vue-vben-admin](https://github.com/vbenjs/vue-vben-admin)
>
> vxe-table [https://gitee.com/xuliangzhan_admin/vxe-table](https://gitee.com/xuliangzhan_admin/vxe-table)
>
> casbin [https://github.com/casbin/casbin](https://github.com/casbin/casbin)
>
> Gorm [https://github.com/go-gorm/gorm](https://github.com/go-gorm/gorm)

## QQ交流群
> ***955576223***
<img width="30%" height="30%" src="https://images.gitee.com/uploads/images/2019/0314/160120_8c5e3e98_1537471.png"/>

## 免责声明：
> 1、Iris Admin仅限自己学习使用，一切商业行为与Iris Admin无关。
>
> 2、用户不得利用Iris Admin从事非法行为，用户应当合法合规的使用，发现用户在使用产品时有任何的非法行为，Iris Admin有权配合有关机关进行调查或向政府部门举报，Iris Admin不承担用户因非法行为造成的任何法律责任，一切法律责任由用户自行承担，如因用户使用造成第三方损害的，用户应当依法予以赔偿。
>
> 3、所有与使用Iris Admin相关的资源直接风险均由用户承担。

## 商用说明
> 商用注意事项
>
> 如果您将此项目用于商业用途，请遵守Apache2.0协议并保留作者技术支持声明。

## 二次开发
> 如果对此项目有好的想法，可联系我做定制开发。
