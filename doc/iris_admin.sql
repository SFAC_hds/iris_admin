/*
 Navicat Premium Data Transfer

 Source Server         : local_202
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 192.168.5.202:3306
 Source Schema         : iris_admin

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 26/07/2022 09:13:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_button
-- ----------------------------
DROP TABLE IF EXISTS `admin_button`;
CREATE TABLE `admin_button`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `created_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creator_id` bigint(0) NULL DEFAULT NULL COMMENT '\'创建者ID\'',
  `updated_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleter_id` bigint(0) NULL DEFAULT NULL COMMENT '\'删除者ID\'',
  `group` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '按钮组',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '按钮名称',
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `parent_id` bigint(0) UNSIGNED NOT NULL COMMENT '父ID',
  `place` bigint(0) NOT NULL DEFAULT 0 COMMENT '按钮的位置',
  `memo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `sort` bigint(0) NOT NULL DEFAULT 0,
  `enable` tinyint(0) NOT NULL DEFAULT 1,
  `tip` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否提示',
  `tip_content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '提示内容',
  `func` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '方法名',
  `disable` tinyint(0) NOT NULL DEFAULT 0 COMMENT '0=不禁用 1=没数量 2=有数量',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_admin_button_deleted_time`(`deleted_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 311570983551057 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_button
-- ----------------------------
INSERT INTO `admin_button` VALUES (311570983551045, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '新建', '', 0, 0, '', 0, 1, 0, '', 'create', 0);
INSERT INTO `admin_button` VALUES (311570983551046, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '编辑', '', 0, 0, '', 0, 1, 0, '', 'edit', 1);
INSERT INTO `admin_button` VALUES (311570983551047, '2022-07-19 12:05:36', 1, '2022-07-19 16:18:38', NULL, 0, '', '删除', '', 0, 0, '', 0, 1, 1, '警告：数据删除后不可恢复！确定删除吗？', 'batchDelete', 2);
INSERT INTO `admin_button` VALUES (311570983551048, '2022-07-19 12:05:36', 1, '2022-07-19 15:12:37', NULL, 0, '', '复制行', '', 0, 0, '', 0, 1, 1, '提示：确定复制该行数据吗？\n2-2oo', 'copy', 1);
INSERT INTO `admin_button` VALUES (311570983551049, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '授权', '', 0, 0, '', 0, 1, 0, '', 'grant', 1);
INSERT INTO `admin_button` VALUES (311570983551050, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '重置密码', '', 0, 0, '', 0, 1, 0, '', 'resetPassword', 1);
INSERT INTO `admin_button` VALUES (311570983551051, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '刷新表', '', 0, 1, '', 0, 1, 0, '', '', 0);
INSERT INTO `admin_button` VALUES (311570983551052, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '导入', '', 0, 1, '', 0, 1, 0, '', '', 0);
INSERT INTO `admin_button` VALUES (311570983551053, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '导出', '', 0, 1, '', 0, 1, 0, '', '', 0);
INSERT INTO `admin_button` VALUES (311570983551054, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '打印', '', 0, 1, '', 0, 1, 0, '', '', 0);
INSERT INTO `admin_button` VALUES (311570983551055, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '最大化', '', 0, 1, '', 0, 1, 0, '', '', 0);
INSERT INTO `admin_button` VALUES (311570983551056, '2022-07-19 12:05:36', 1, '', NULL, 0, '', '自定义列', '', 0, 1, '', 0, 1, 0, '', '', 0);

-- ----------------------------
-- Table structure for admin_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_log`;
CREATE TABLE `admin_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `created_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creator_id` bigint(0) NULL DEFAULT NULL COMMENT '\'创建者ID\'',
  `updated_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleter_id` bigint(0) NULL DEFAULT NULL COMMENT '\'删除者ID\'',
  `path` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `method` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `params` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `data` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `group_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `duration` bigint(0) NOT NULL DEFAULT 0,
  `ip_addr` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `os` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `user_agent` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `resp_status_code` bigint(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_admin_log_deleted_time`(`deleted_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 313039732121670 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_log
-- ----------------------------
INSERT INTO `admin_log` VALUES (313037534552134, '2022-07-23 15:33:01', 1, '', NULL, 0, '/api/admin/user/table', 'GET', '{\"_t\":\"1658561581214\",\"currentPage\":\"1\",\"pageSize\":\"50\",\"total\":\"3\"}', '', '用户', '管理报表', 4, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313037542993989, '2022-07-23 15:33:03', 1, '', NULL, 0, '/api/admin/user/table', 'GET', '{\"_t\":\"1658561582147\",\"currentPage\":\"1\",\"pageSize\":\"50\",\"total\":\"0\"}', '', '用户', '管理报表', 4, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038384152645, '2022-07-23 15:36:28', 1, '', NULL, 0, '/api/admin/policy/table', 'GET', '{\"_t\":\"1658561786033\",\"currentPage\":\"1\",\"pageSize\":\"50\",\"total\":\"0\"}', '', '接口策略', '管理报表', 2, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038384152646, '2022-07-23 15:36:28', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561788485\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"40\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038386085957, '2022-07-23 15:36:29', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561788734\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"40\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038386085958, '2022-07-23 15:36:29', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561788961\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"42\"}', '', '操作日志', '管理报表', 4, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038389145669, '2022-07-23 15:36:29', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561789327\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"42\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038389145670, '2022-07-23 15:36:29', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561789700\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"44\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038392131653, '2022-07-23 15:36:30', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561790070\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"44\"}', '', '操作日志', '管理报表', 4, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038392131654, '2022-07-23 15:36:30', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561790452\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"46\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038396608581, '2022-07-23 15:36:31', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561790838\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"46\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038396608582, '2022-07-23 15:36:31', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561791594\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"0\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038444568645, '2022-07-23 15:36:43', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561798157\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"48\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038444568646, '2022-07-23 15:36:43', 1, '', NULL, 0, '/api/admin/policy/table', 'GET', '{\"_t\":\"1658561803297\",\"currentPage\":\"1\",\"pageSize\":\"50\",\"total\":\"0\"}', '', '接口策略', '管理报表', 2, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038454902853, '2022-07-23 15:36:46', 1, '', NULL, 0, '/api/admin/menu/list', 'GET', '{\"_t\":\"1658561804560\"}', '', '菜单', '列表', 24, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038454902854, '2022-07-23 15:36:46', 1, '', NULL, 0, '/api/admin/menu/list', 'GET', '{\"_t\":\"1658561805791\"}', '', '菜单', '列表', 37, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038467432517, '2022-07-23 15:36:49', 1, '', NULL, 0, '/api/admin/menu/list', 'GET', '{\"_t\":\"1658561806005\"}', '', '菜单', '列表', 21, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038467432518, '2022-07-23 15:36:49', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561808825\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"50\"}', '', '操作日志', '管理报表', 4, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038489129029, '2022-07-23 15:36:54', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561810662\",\"currentPage\":\"1\",\"groupName\":\"菜单\",\"pageSize\":\"20\",\"total\":\"54\"}', '', '操作日志', '管理报表', 9, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038489129030, '2022-07-23 15:36:54', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561814182\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"3\"}', '', '操作日志', '管理报表', 11, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038540877893, '2022-07-23 15:37:07', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561821925\",\"currentPage\":\"1\",\"name\":\"列表\",\"pageSize\":\"20\",\"total\":\"56\"}', '', '操作日志', '管理报表', 7, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038540877894, '2022-07-23 15:37:07', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561826813\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"3\"}', '', '操作日志', '管理报表', 7, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038830596165, '2022-07-23 15:38:17', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561894323\",\"currentPage\":\"1\",\"pageSize\":\"20\",\"total\":\"58\"}', '', '操作日志', '管理报表', 10, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038830596166, '2022-07-23 15:38:17', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561896488\",\"currentPage\":\"1\",\"groupName\":\"按钮\",\"pageSize\":\"20\",\"total\":\"60\"}', '', '操作日志', '管理报表', 7, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038863061061, '2022-07-23 15:38:25', 1, '', NULL, 0, '/api/admin/button/list', 'GET', '{\"_t\":\"1658561901345\"}', '', '按钮', '列表', 2, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038863061062, '2022-07-23 15:38:25', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561905463\",\"currentPage\":\"1\",\"groupName\":\"角色\",\"pageSize\":\"20\",\"total\":\"0\"}', '', '操作日志', '管理报表', 8, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038874513477, '2022-07-23 15:38:28', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561906632\",\"currentPage\":\"1\",\"groupName\":\"菜单\",\"pageSize\":\"20\",\"total\":\"0\"}', '', '操作日志', '管理报表', 8, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038874513478, '2022-07-23 15:38:28', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561908280\",\"currentPage\":\"1\",\"groupName\":\"角色\",\"pageSize\":\"20\",\"total\":\"3\"}', '', '操作日志', '管理报表', 8, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038884679749, '2022-07-23 15:38:30', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561909559\",\"currentPage\":\"1\",\"groupName\":\"用户\",\"pageSize\":\"20\",\"total\":\"0\"}', '', '操作日志', '管理报表', 13, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038884679750, '2022-07-23 15:38:30', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561910768\",\"currentPage\":\"1\",\"groupName\":\"菜单\",\"pageSize\":\"20\",\"total\":\"40\"}', '', '操作日志', '管理报表', 8, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313038914244677, '2022-07-23 15:38:38', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658561912032\",\"currentPage\":\"1\",\"groupName\":\"按钮\",\"pageSize\":\"20\",\"total\":\"3\"}', '', '操作日志', '管理报表', 8, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313039422787653, '2022-07-23 15:40:42', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658562035917\",\"currentPage\":\"1\",\"groupName\":\"按钮\",\"pageSize\":\"20\",\"total\":\"1\"}', '', '操作日志', '管理报表', 4, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313039682428997, '2022-07-23 15:41:45', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658562102943\",\"currentPage\":\"1\",\"groupName\":\"菜单\",\"pageSize\":\"20\",\"total\":\"1\"}', '', '操作日志', '管理报表', 8, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313039682428998, '2022-07-23 15:41:45', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658562104326\",\"currentPage\":\"1\",\"groupName\":\"按钮\",\"pageSize\":\"20\",\"total\":\"3\"}', '', '操作日志', '管理报表', 5, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313039687675973, '2022-07-23 15:41:46', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658562105566\",\"currentPage\":\"1\",\"groupName\":\"用户\",\"pageSize\":\"20\",\"total\":\"1\"}', '', '操作日志', '管理报表', 11, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313039687675974, '2022-07-23 15:41:46', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658562106806\",\"currentPage\":\"1\",\"groupName\":\"菜单\",\"pageSize\":\"20\",\"total\":\"40\"}', '', '操作日志', '管理报表', 6, '::1', '重庆市', 'Linux', 'Chrome', 200);
INSERT INTO `admin_log` VALUES (313039732121669, '2022-07-23 15:41:57', 1, '', NULL, 0, '/api/admin/log/table', 'GET', '{\"_t\":\"1658562111572\",\"currentPage\":\"1\",\"groupName\":\"用户\",\"pageSize\":\"20\",\"total\":\"40\"}', '', '操作日志', '管理报表', 6, '::1', '重庆市', 'Linux', 'Chrome', 200);

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `path` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `component` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `redirect` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `meta_id` bigint(0) UNSIGNED NULL DEFAULT NULL,
  `parent_id` bigint(0) UNSIGNED NOT NULL DEFAULT 0,
  `sort` bigint(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 312972943257670 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES (308698753335402, 'button', 'Button', '/sys_setting/sys/button/index', '', 311572006785093, 308698753335410, 0);
INSERT INTO `admin_menu` VALUES (308698753335403, 'log', 'Log', '/sys_setting/sys/log/index', '', 17, 308698753335410, 0);
INSERT INTO `admin_menu` VALUES (308698753335404, 'statsviz', 'Statsviz', 'IFrame', '', 15, 308698753335410, 0);
INSERT INTO `admin_menu` VALUES (308698753335405, 'user', 'User', '/sys_setting/admin/user/index', '', 311636372672581, 308698753335409, 0);
INSERT INTO `admin_menu` VALUES (308698753335406, 'policy', 'Policy', '/sys_setting/sys/policy/index', '', 308736032854085, 308698753335410, 0);
INSERT INTO `admin_menu` VALUES (308698753335407, 'role', 'Role', '/sys_setting/admin/role/index', '', 311614648979525, 308698753335409, 0);
INSERT INTO `admin_menu` VALUES (308698753335408, 'menu', 'Menu', '/sys_setting/sys/menu/index', '', 311608265478213, 308698753335410, 0);
INSERT INTO `admin_menu` VALUES (308698753335409, '/admin', 'Admin', 'LAYOUT', '', 22, 308698753335411, 0);
INSERT INTO `admin_menu` VALUES (308698753335410, '/sys', 'Sys', 'LAYOUT', '', 312942302019653, 308698753335411, 2);
INSERT INTO `admin_menu` VALUES (308698753335411, '/sys_setting', 'Admin', 'LAYOUT', '', 308811870249029, 0, 1);
INSERT INTO `admin_menu` VALUES (308698753335412, 'about', 'About', '/sys/about/index', '', 3, 308698753335414, 0);
INSERT INTO `admin_menu` VALUES (308698753335413, 'analysis', 'Analysis', '/dashboard/analysis/index', '', 2, 308698753335414, 0);
INSERT INTO `admin_menu` VALUES (308698753335414, '/dashboard', 'Dashboard', 'LAYOUT', '/dashboard/analysis', 1, 0, 0);
INSERT INTO `admin_menu` VALUES (312971901177925, 'org', 'Org', '/sys_setting/admin/org/index', '', 312974347718725, 308698753335409, 0);

-- ----------------------------
-- Table structure for admin_menu_button
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu_button`;
CREATE TABLE `admin_menu_button`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mid` bigint(0) UNSIGNED NOT NULL,
  `bid` bigint(0) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 312974347735115 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_menu_button
-- ----------------------------
INSERT INTO `admin_menu_button` VALUES (311572006797381, 308698753335402, 311570983551046);
INSERT INTO `admin_menu_button` VALUES (311608265486405, 308698753335408, 311570983551045);
INSERT INTO `admin_menu_button` VALUES (311608265486406, 308698753335408, 311570983551047);
INSERT INTO `admin_menu_button` VALUES (311608265486407, 308698753335408, 311570983551051);
INSERT INTO `admin_menu_button` VALUES (311608265486408, 308698753335408, 311570983551055);
INSERT INTO `admin_menu_button` VALUES (311608265486409, 308698753335408, 311570983551056);
INSERT INTO `admin_menu_button` VALUES (311608265486410, 308698753335408, 311570983551054);
INSERT INTO `admin_menu_button` VALUES (311608265486411, 308698753335408, 311570983551048);
INSERT INTO `admin_menu_button` VALUES (311614648987717, 308698753335407, 311570983551045);
INSERT INTO `admin_menu_button` VALUES (311614648987718, 308698753335407, 311570983551046);
INSERT INTO `admin_menu_button` VALUES (311614648987719, 308698753335407, 311570983551047);
INSERT INTO `admin_menu_button` VALUES (311614648987720, 308698753335407, 311570983551049);
INSERT INTO `admin_menu_button` VALUES (311614648987721, 308698753335407, 311570983551051);
INSERT INTO `admin_menu_button` VALUES (311614648987722, 308698753335407, 311570983551054);
INSERT INTO `admin_menu_button` VALUES (311614648987723, 308698753335407, 311570983551055);
INSERT INTO `admin_menu_button` VALUES (311614648987724, 308698753335407, 311570983551056);
INSERT INTO `admin_menu_button` VALUES (311636372684869, 308698753335405, 311570983551045);
INSERT INTO `admin_menu_button` VALUES (311636372684870, 308698753335405, 311570983551046);
INSERT INTO `admin_menu_button` VALUES (311636372684871, 308698753335405, 311570983551047);
INSERT INTO `admin_menu_button` VALUES (311636372684872, 308698753335405, 311570983551051);
INSERT INTO `admin_menu_button` VALUES (311636372684873, 308698753335405, 311570983551054);
INSERT INTO `admin_menu_button` VALUES (311636372684874, 308698753335405, 311570983551055);
INSERT INTO `admin_menu_button` VALUES (311636372684875, 308698753335405, 311570983551056);
INSERT INTO `admin_menu_button` VALUES (311636372684876, 308698753335405, 311570983551050);
INSERT INTO `admin_menu_button` VALUES (312974347735109, 312971901177925, 311570983551045);
INSERT INTO `admin_menu_button` VALUES (312974347735110, 312971901177925, 311570983551046);
INSERT INTO `admin_menu_button` VALUES (312974347735111, 312971901177925, 311570983551047);
INSERT INTO `admin_menu_button` VALUES (312974347735112, 312971901177925, 311570983551051);
INSERT INTO `admin_menu_button` VALUES (312974347735113, 312971901177925, 311570983551055);
INSERT INTO `admin_menu_button` VALUES (312974347735114, 312971901177925, 311570983551056);

-- ----------------------------
-- Table structure for admin_menu_meta
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu_meta`;
CREATE TABLE `admin_menu_meta`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_no` bigint(0) NOT NULL DEFAULT 0,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `dynamic_level` bigint(0) NOT NULL DEFAULT 0,
  `real_path` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `ignore_auth` tinyint(1) NOT NULL DEFAULT 0,
  `ignore_keep_alive` tinyint(1) NOT NULL DEFAULT 0,
  `affix` tinyint(1) NOT NULL DEFAULT 0,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `frame_src` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `transition_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `hide_breadcrumb` tinyint(1) NOT NULL DEFAULT 0,
  `hide_children_in_menu` tinyint(1) NOT NULL DEFAULT 0,
  `carry_param` tinyint(1) NOT NULL DEFAULT 0,
  `single` tinyint(1) NOT NULL DEFAULT 0,
  `current_active_menu` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `hide_tab` tinyint(1) NOT NULL DEFAULT 0,
  `hide_menu` tinyint(1) NOT NULL DEFAULT 0,
  `is_link` tinyint(1) NOT NULL DEFAULT 0,
  `ignore_route` tinyint(1) NOT NULL DEFAULT 0,
  `hide_path_for_children` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 312974347718726 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_menu_meta
-- ----------------------------
INSERT INTO `admin_menu_meta` VALUES (1, 0, 'routes.dashboard.dashboard', 0, '', 0, 0, 0, 'twemoji:wolf', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (2, 0, 'routes.dashboard.analysis', 0, '', 0, 0, 0, 'akar-icons:home', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (3, 0, 'routes.dashboard.about', 0, '', 0, 0, 0, 'flat-color-icons:about', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (7, 0, 'routes.sysSetting.default', 0, '', 0, 0, 0, 'icon-park-outline:system', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (8, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (9, 0, 'routes.sysSetting.admin.role', 0, '', 0, 0, 0, 'twemoji:cowboy-hat-face', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (10, 0, 'routes.sysSetting.sys.policy', 0, '', 0, 0, 0, 'twemoji:whale', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (11, 0, 'routes.sysSetting.admin.user', 0, '', 0, 0, 0, 'icon-park-twotone:peoples', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (15, 0, 'routes.sysSetting.sys.monitor', 0, '', 0, 0, 0, 'tabler:activity-heartbeat', 'http://localhost:10087/statsviz/', '', 0, 0, 0, 0, '', 0, 0, 1, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (17, 0, 'routes.sysSetting.sys.log', 0, '', 0, 0, 0, 'fxemoji:dromedarycamel', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (22, 0, 'routes.sysSetting.admin.admin', 0, '', 0, 0, 0, 'simple-icons:googlemarketingplatform', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (23, 0, 'routes.sysSetting.sys.sys', 0, '', 0, 0, 0, 'carbon:cics-system-group', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (27, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308715792179269, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308721702113349, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308722019508293, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308734431432773, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308735908024389, 0, 'routes.sysSetting.admin.user', 0, '', 0, 0, 0, 'icon-park-twotone:peoples', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308735956090949, 0, 'routes.sysSetting.admin.role', 0, '', 0, 0, 0, 'twemoji:cowboy-hat-face', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308736032854085, 0, 'routes.sysSetting.sys.policy', 0, '', 0, 0, 0, 'twemoji:whale', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308738914603077, 0, 'routes.sysSetting.admin.role', 0, '', 0, 0, 0, 'twemoji:cowboy-hat-face', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (308811870249029, 0, 'routes.sysSetting.default', 0, '', 0, 0, 0, 'icon-park-outline:system', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (309505582219333, 0, 'routes.sysSetting.admin.user', 0, '', 0, 0, 0, 'icon-park-twotone:peoples', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311216529604677, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311565816594501, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311565999890501, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311566234218565, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311566623207493, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311566802772037, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311567402156101, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311567524859973, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311567821275205, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311567915765829, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311568266645573, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311571073024069, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311572006785093, 0, 'routes.sysSetting.sys.button', 0, '', 0, 0, 0, 'icon-park-outline:switch-button', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311593468543045, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311593561342021, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311602130550853, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311604782882885, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311605129429061, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311605556650053, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311605608366149, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311606101184581, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311606323880005, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311606553854021, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311606673535045, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311607411970117, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311607457554501, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311607488589893, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311607752515653, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311608265478213, 0, 'routes.sysSetting.sys.menu', 0, '', 0, 0, 0, 'twemoji:yellow-heart', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311614648979525, 0, 'routes.sysSetting.admin.role', 0, '', 0, 0, 0, 'twemoji:cowboy-hat-face', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311636334563397, 0, 'routes.sysSetting.admin.user', 0, '', 0, 0, 0, 'icon-park-twotone:peoples', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (311636372672581, 0, 'routes.sysSetting.admin.user', 0, '', 0, 0, 0, 'icon-park-twotone:peoples', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (312942302019653, 0, 'routes.sysSetting.sys.sys', 0, '', 0, 0, 0, 'carbon:cics-system-group', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (312971901173829, 0, 'routes.sysSetting.admin.role', 0, '', 0, 0, 0, 'twemoji:cowboy-hat-face', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (312972378992709, 0, 'routes.sysSetting.admin.org', 0, '', 0, 0, 0, 'twemoji:cowboy-hat-face', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (312972550230085, 0, 'routes.sysSetting.admin.org', 0, '', 0, 0, 0, 'twemoji:cowboy-hat-face', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (312973368090693, 0, 'routes.sysSetting.admin.org', 0, '', 0, 0, 0, 'ps:organisation', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `admin_menu_meta` VALUES (312974347718725, 0, 'routes.sysSetting.admin.org', 0, '', 0, 0, 0, 'ps:organisation', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for admin_org
-- ----------------------------
DROP TABLE IF EXISTS `admin_org`;
CREATE TABLE `admin_org`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `created_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creator_id` bigint(0) NULL DEFAULT NULL COMMENT '\'创建者ID\'',
  `updated_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleter_id` bigint(0) NULL DEFAULT NULL COMMENT '\'删除者ID\'',
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `area` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `office_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `register_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `person` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `memo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `enable` tinyint(0) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE,
  INDEX `idx_admin_org_deleted_time`(`deleted_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 313029966458950 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_org
-- ----------------------------
INSERT INTO `admin_org` VALUES (312981200740421, '2022-07-23 11:43:47', 0, '2022-07-23 16:13:44', NULL, 0, 'A01', '集团公司', '12,1201,120102,120102002', '', '', '121', '', '德萨德萨的', 1);
INSERT INTO `admin_org` VALUES (312981278031941, '2022-07-23 11:44:06', 0, '2022-07-25 16:43:37', NULL, 0, 'A02', '成都子公司', '23,2302,230203,230203006', '', '', '', '', '反对', 0);

-- ----------------------------
-- Table structure for admin_policy
-- ----------------------------
DROP TABLE IF EXISTS `admin_policy`;
CREATE TABLE `admin_policy`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `created_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creator_id` bigint(0) NULL DEFAULT NULL COMMENT '\'创建者ID\'',
  `updated_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleter_id` bigint(0) NULL DEFAULT NULL COMMENT '\'删除者ID\'',
  `group_name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `memo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `is_system` tinyint(1) NOT NULL DEFAULT 1,
  `v1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v4` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v5` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `logable` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_admin_policy_deleted_time`(`deleted_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 308736336310342 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_policy
-- ----------------------------
INSERT INTO `admin_policy` VALUES (308702148550725, '2022-07-11 09:32:17', 0, '', NULL, 0, '用户', 'login', '', 1, 'tenant_admin', '/api/admin/user/login', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148554821, '2022-07-11 09:32:17', 0, '', NULL, 0, '用户', '获取用户信息', '', 1, 'tenant_admin', '/api/admin/user/getUserInfo', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148563013, '2022-07-11 09:32:17', 0, '', NULL, 0, '用户', '重置密码', '', 1, 'tenant_admin', '/api/admin/user/ResetPassword', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148567109, '2022-07-11 09:32:17', 0, '', NULL, 0, '用户', '创建', '', 1, 'tenant_admin', '/api/admin/user', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148571205, '2022-07-11 09:32:17', 0, '', NULL, 0, '用户', '删除', '', 1, 'tenant_admin', '/api/admin/user', 'DELETE', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148575301, '2022-07-11 09:32:17', 0, '', NULL, 0, '用户', '修改', '', 1, 'tenant_admin', '/api/admin/user', 'PUT', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148583493, '2022-07-11 09:32:17', 0, '', NULL, 0, '用户', '管理报表', '', 1, 'tenant_admin', '/api/admin/user/table', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148587589, '2022-07-11 09:32:17', 0, '', NULL, 0, '菜单', '创建', '', 1, 'tenant_admin', '/api/admin/menu', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148591685, '2022-07-11 09:32:17', 0, '', NULL, 0, '菜单', '删除', '', 1, 'tenant_admin', '/api/admin/menu', 'DELETE', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148595781, '2022-07-11 09:32:17', 0, '', NULL, 0, '菜单', '修改', '', 1, 'tenant_admin', '/api/admin/menu', 'PUT', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148603973, '2022-07-11 09:32:17', 0, '', NULL, 0, '菜单', '列表', '', 1, 'tenant_admin', '/api/admin/menu/list', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148608069, '2022-07-11 09:32:17', 0, '', NULL, 0, '菜单', '拥有的列表', '', 1, 'tenant_admin', '/api/admin/menu/ownList', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148612165, '2022-07-11 09:32:17', 0, '', NULL, 0, '按钮', '创建', '', 1, 'tenant_admin', '/api/admin/button', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148616261, '2022-07-11 09:32:17', 0, '', NULL, 0, '按钮', '生成按钮', '', 1, 'tenant_admin', '/api/admin/button/generate', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148624453, '2022-07-11 09:32:17', 0, '', NULL, 0, '按钮', '删除', '', 1, 'tenant_admin', '/api/admin/button', 'DELETE', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148628549, '2022-07-11 09:32:17', 0, '', NULL, 0, '按钮', '修改', '', 1, 'tenant_admin', '/api/admin/button', 'PUT', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148636741, '2022-07-11 09:32:17', 0, '', NULL, 0, '按钮', '列表', '', 1, 'tenant_admin', '/api/admin/button/list', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148640837, '2022-07-11 09:32:17', 0, '', NULL, 0, '按钮', 'code列表', '', 1, 'tenant_admin', '/api/admin/button/codeList', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148649029, '2022-07-11 09:32:17', 0, '', NULL, 0, '角色', '创建', '', 1, 'tenant_admin', '/api/admin/role', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148653125, '2022-07-11 09:32:17', 0, '', NULL, 0, '角色', '删除', '', 1, 'tenant_admin', '/api/admin/role', 'DELETE', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148661317, '2022-07-11 09:32:17', 0, '', NULL, 0, '角色', '修改', '', 1, 'tenant_admin', '/api/admin/role', 'PUT', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148665413, '2022-07-11 09:32:17', 0, '', NULL, 0, '角色', '管理报表', '', 1, 'tenant_admin', '/api/admin/role/table', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148669509, '2022-07-11 09:32:17', 0, '', NULL, 0, '角色', '列表', '', 1, 'tenant_admin', '/api/admin/role/list', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148673605, '2022-07-11 09:32:17', 0, '', NULL, 0, '角色', '授权', '', 1, 'tenant_admin', '/api/admin/role/grant', 'POST', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148677701, '2022-07-11 09:32:17', 0, '', NULL, 0, '角色', '已有的权限', '', 1, 'tenant_admin', '/api/admin/role/grant/owned', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148681797, '2022-07-11 09:32:17', 0, '', NULL, 0, '接口策略', '删除', '', 1, 'tenant_admin', '/api/admin/policy', 'DELETE', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148685893, '2022-07-11 09:32:17', 0, '', NULL, 0, '接口策略', '管理报表', '', 1, 'tenant_admin', '/api/admin/policy/table', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148689989, '2022-07-11 09:32:17', 0, '', NULL, 0, '接口策略', '重载策略', '', 1, 'tenant_admin', '/api/admin/policy/reloadPolicy', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148689990, '2022-07-11 09:32:17', 0, '', NULL, 0, '接口策略', '列表', '', 1, 'tenant_admin', '/api/admin/policy/list', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148694085, '2022-07-11 09:32:17', 0, '', NULL, 0, '接口策略', '日志埋点', '', 1, 'tenant_admin', '/api/admin/policy/log', 'PUT', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148702277, '2022-07-11 09:32:17', 0, '', NULL, 0, '操作日志', '删除', '', 1, 'tenant_admin', '/api/admin/log', 'DELETE', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308702148706373, '2022-07-11 09:32:17', 0, '', NULL, 0, '操作日志', '管理报表', '', 1, 'tenant_admin', '/api/admin/log/table', 'GET', '.*', '', 0);
INSERT INTO `admin_policy` VALUES (308736336310341, '2022-07-11 11:51:24', 0, '', NULL, 0, '用户', 'logout', '', 1, 'tenant_admin', '/api/admin/user/logout', 'GET', '.*', '', 0);

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `created_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creator_id` bigint(0) NULL DEFAULT NULL COMMENT '\'创建者ID\'',
  `updated_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleter_id` bigint(0) NULL DEFAULT NULL COMMENT '\'删除者ID\'',
  `name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `memo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_admin_role_deleted_time`(`deleted_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 312939538423878 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES (308740683833413, '2022-07-11 12:09:05', 0, '', NULL, 0, 'admin', '');
INSERT INTO `admin_role` VALUES (308740709306437, '', 0, '2022-07-19 15:10:45', NULL, 0, 'ceshi', '223213132222');
INSERT INTO `admin_role` VALUES (312939511799877, '2022-07-23 08:54:09', 0, '', NULL, 0, '菜单权限', '');
INSERT INTO `admin_role` VALUES (312939538423877, '2022-07-23 08:54:16', 0, '', NULL, 0, '接口权限', '');

-- ----------------------------
-- Table structure for admin_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rid` bigint(0) UNSIGNED NOT NULL,
  `mid` bigint(0) UNSIGNED NOT NULL,
  `bid` bigint(0) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 312941452427341 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu` VALUES (308740806221894, 308740683833413, 308698753335411, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221895, 308740683833413, 308698753335409, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221896, 308740683833413, 308698753335405, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221897, 308740683833413, 308698753335407, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221898, 308740683833413, 308698753335410, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221899, 308740683833413, 308698753335402, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221900, 308740683833413, 308698753335403, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221901, 308740683833413, 308698753335404, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221902, 308740683833413, 308698753335406, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221903, 308740683833413, 308698753335408, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221904, 308740683833413, 308698753335414, 0);
INSERT INTO `admin_role_menu` VALUES (308740806221905, 308740683833413, 308698753335413, 0);
INSERT INTO `admin_role_menu` VALUES (309052222652485, 308740709306437, 308698753335414, 0);
INSERT INTO `admin_role_menu` VALUES (309052222652486, 308740709306437, 308698753335413, 0);
INSERT INTO `admin_role_menu` VALUES (309052222652487, 308740709306437, 308698753335411, 0);
INSERT INTO `admin_role_menu` VALUES (309052222652488, 308740709306437, 308698753335409, 0);
INSERT INTO `admin_role_menu` VALUES (309052222652489, 308740709306437, 308698753335405, 0);
INSERT INTO `admin_role_menu` VALUES (312941452427333, 312939511799877, 308698753335414, 0);
INSERT INTO `admin_role_menu` VALUES (312941452427334, 312939511799877, 308698753335413, 0);
INSERT INTO `admin_role_menu` VALUES (312941452427335, 312939511799877, 308698753335411, 0);
INSERT INTO `admin_role_menu` VALUES (312941452427336, 312939511799877, 308698753335410, 0);
INSERT INTO `admin_role_menu` VALUES (312941452427337, 312939511799877, 308698753335408, 311570983551051);
INSERT INTO `admin_role_menu` VALUES (312941452427338, 312939511799877, 308698753335408, 311570983551055);
INSERT INTO `admin_role_menu` VALUES (312941452427339, 312939511799877, 308698753335408, 311570983551056);
INSERT INTO `admin_role_menu` VALUES (312941452427340, 312939511799877, 308698753335408, 311570983551054);

-- ----------------------------
-- Table structure for admin_role_policy
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_policy`;
CREATE TABLE `admin_role_policy`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rid` bigint(0) UNSIGNED NOT NULL,
  `pid` bigint(0) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 312940022632518 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_policy
-- ----------------------------
INSERT INTO `admin_role_policy` VALUES (308740806180933, 308740683833413, 308702148550725);
INSERT INTO `admin_role_policy` VALUES (308740806185029, 308740683833413, 308702148554821);
INSERT INTO `admin_role_policy` VALUES (308740806185030, 308740683833413, 308702148563013);
INSERT INTO `admin_role_policy` VALUES (308740806185031, 308740683833413, 308702148567109);
INSERT INTO `admin_role_policy` VALUES (308740806185032, 308740683833413, 308702148571205);
INSERT INTO `admin_role_policy` VALUES (308740806185033, 308740683833413, 308702148575301);
INSERT INTO `admin_role_policy` VALUES (308740806189125, 308740683833413, 308702148583493);
INSERT INTO `admin_role_policy` VALUES (308740806189126, 308740683833413, 308702148587589);
INSERT INTO `admin_role_policy` VALUES (308740806189127, 308740683833413, 308702148591685);
INSERT INTO `admin_role_policy` VALUES (308740806193221, 308740683833413, 308702148595781);
INSERT INTO `admin_role_policy` VALUES (308740806193222, 308740683833413, 308702148603973);
INSERT INTO `admin_role_policy` VALUES (308740806193223, 308740683833413, 308702148608069);
INSERT INTO `admin_role_policy` VALUES (308740806193224, 308740683833413, 308702148612165);
INSERT INTO `admin_role_policy` VALUES (308740806197317, 308740683833413, 308702148616261);
INSERT INTO `admin_role_policy` VALUES (308740806197318, 308740683833413, 308702148624453);
INSERT INTO `admin_role_policy` VALUES (308740806197319, 308740683833413, 308702148628549);
INSERT INTO `admin_role_policy` VALUES (308740806201413, 308740683833413, 308702148636741);
INSERT INTO `admin_role_policy` VALUES (308740806201414, 308740683833413, 308702148640837);
INSERT INTO `admin_role_policy` VALUES (308740806201415, 308740683833413, 308702148649029);
INSERT INTO `admin_role_policy` VALUES (308740806205509, 308740683833413, 308702148653125);
INSERT INTO `admin_role_policy` VALUES (308740806205510, 308740683833413, 308702148661317);
INSERT INTO `admin_role_policy` VALUES (308740806205511, 308740683833413, 308702148665413);
INSERT INTO `admin_role_policy` VALUES (308740806205512, 308740683833413, 308702148669509);
INSERT INTO `admin_role_policy` VALUES (308740806209605, 308740683833413, 308702148673605);
INSERT INTO `admin_role_policy` VALUES (308740806209606, 308740683833413, 308702148677701);
INSERT INTO `admin_role_policy` VALUES (308740806209607, 308740683833413, 308702148681797);
INSERT INTO `admin_role_policy` VALUES (308740806213701, 308740683833413, 308702148685893);
INSERT INTO `admin_role_policy` VALUES (308740806213702, 308740683833413, 308702148689989);
INSERT INTO `admin_role_policy` VALUES (308740806213703, 308740683833413, 308702148689990);
INSERT INTO `admin_role_policy` VALUES (308740806217797, 308740683833413, 308702148694085);
INSERT INTO `admin_role_policy` VALUES (308740806217798, 308740683833413, 308702148702277);
INSERT INTO `admin_role_policy` VALUES (308740806217799, 308740683833413, 308702148706373);
INSERT INTO `admin_role_policy` VALUES (308740806221893, 308740683833413, 308736336310341);
INSERT INTO `admin_role_policy` VALUES (309052222648389, 308740709306437, 308702148583493);
INSERT INTO `admin_role_policy` VALUES (309052222648390, 308740709306437, 308702148636741);
INSERT INTO `admin_role_policy` VALUES (312940022632517, 312939538423877, 308702148603973);

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `created_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creator_id` bigint(0) NULL DEFAULT NULL COMMENT '\'创建者ID\'',
  `updated_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleter_id` bigint(0) NULL DEFAULT NULL COMMENT '\'删除者ID\'',
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `gender` tinyint(0) NOT NULL DEFAULT 0,
  `enable` tinyint(0) NOT NULL DEFAULT 1,
  `age` bigint(0) NULL DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `memo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `org_id` bigint(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE,
  INDEX `idx_admin_user_deleted_time`(`deleted_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 309169507098694 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES (1, '', 0, '', NULL, 0, 'root', '$2a$04$XaBVmVugrGhPcnjNAJM6JutY.MUp6Em9dBl8/FaAaW6YDePLGJb0S', '超神', 0, 1, 0, '17833', '', '', '', 0);
INSERT INTO `admin_user` VALUES (308736185663557, '2022-07-11 11:50:47', 0, '', NULL, 0, 'yhm', '$2a$04$TXZzDfTWw3WmhVge2sjoJupleQrrLyQRLDD/UnyvSLRKUVxOsX2RS', '中单', 0, 1, 0, '111', '', '', '', 0);
INSERT INTO `admin_user` VALUES (308738821750853, '2022-07-11 12:01:30', 0, '2022-07-26 09:05:34', NULL, 0, 'daye', '$2a$04$r0kU3qjmWZwgUi.6FJ19v.PcT.CwBVN24CsnDzPOsObtE0f/LUPrq', '打野', 0, 1, 0, '133332', '', '', '', 312981200740421);

-- ----------------------------
-- Table structure for casbin_rule
-- ----------------------------
DROP TABLE IF EXISTS `casbin_rule`;
CREATE TABLE `casbin_rule`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ptype` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v0` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v4` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `v5` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 313045112418375 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of casbin_rule
-- ----------------------------
INSERT INTO `casbin_rule` VALUES (308740806176837, 'p', '308740683833413', 'tenant_admin', '/api/admin/user/login', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176838, 'p', '308740683833413', 'tenant_admin', '/api/admin/user/getUserInfo', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176839, 'p', '308740683833413', 'tenant_admin', '/api/admin/user/ResetPassword', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176840, 'p', '308740683833413', 'tenant_admin', '/api/admin/user', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176841, 'p', '308740683833413', 'tenant_admin', '/api/admin/user', 'DELETE', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176842, 'p', '308740683833413', 'tenant_admin', '/api/admin/user', 'PUT', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176843, 'p', '308740683833413', 'tenant_admin', '/api/admin/user/table', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176844, 'p', '308740683833413', 'tenant_admin', '/api/admin/menu', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176845, 'p', '308740683833413', 'tenant_admin', '/api/admin/menu', 'DELETE', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176846, 'p', '308740683833413', 'tenant_admin', '/api/admin/menu', 'PUT', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176847, 'p', '308740683833413', 'tenant_admin', '/api/admin/menu/list', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176848, 'p', '308740683833413', 'tenant_admin', '/api/admin/menu/ownList', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176849, 'p', '308740683833413', 'tenant_admin', '/api/admin/button', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176850, 'p', '308740683833413', 'tenant_admin', '/api/admin/button/generate', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176851, 'p', '308740683833413', 'tenant_admin', '/api/admin/button', 'DELETE', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176852, 'p', '308740683833413', 'tenant_admin', '/api/admin/button', 'PUT', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176853, 'p', '308740683833413', 'tenant_admin', '/api/admin/button/list', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176854, 'p', '308740683833413', 'tenant_admin', '/api/admin/button/codeList', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176855, 'p', '308740683833413', 'tenant_admin', '/api/admin/role', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176856, 'p', '308740683833413', 'tenant_admin', '/api/admin/role', 'DELETE', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176857, 'p', '308740683833413', 'tenant_admin', '/api/admin/role', 'PUT', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176858, 'p', '308740683833413', 'tenant_admin', '/api/admin/role/table', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176859, 'p', '308740683833413', 'tenant_admin', '/api/admin/role/list', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176860, 'p', '308740683833413', 'tenant_admin', '/api/admin/role/grant', 'POST', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176861, 'p', '308740683833413', 'tenant_admin', '/api/admin/role/grant/owned', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176862, 'p', '308740683833413', 'tenant_admin', '/api/admin/policy', 'DELETE', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176863, 'p', '308740683833413', 'tenant_admin', '/api/admin/policy/table', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176864, 'p', '308740683833413', 'tenant_admin', '/api/admin/policy/reloadPolicy', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176865, 'p', '308740683833413', 'tenant_admin', '/api/admin/policy/list', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176866, 'p', '308740683833413', 'tenant_admin', '/api/admin/policy/log', 'PUT', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176867, 'p', '308740683833413', 'tenant_admin', '/api/admin/log', 'DELETE', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176868, 'p', '308740683833413', 'tenant_admin', '/api/admin/log/table', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740806176869, 'p', '308740683833413', 'tenant_admin', '/api/admin/user/logout', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (308740915220549, 'g', '308736185663557', '308740683833413', 'tenant_admin', '', '', '');
INSERT INTO `casbin_rule` VALUES (309052222644293, 'p', '308740709306437', 'tenant_admin', '/api/admin/user/table', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (309052222644294, 'p', '308740709306437', 'tenant_admin', '/api/admin/button/list', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (312940022628421, 'p', '312939538423877', 'tenant_admin', '/api/admin/menu/list', 'GET', '.*', '');
INSERT INTO `casbin_rule` VALUES (314003998474309, 'g', '308738821750853', '312939511799877', 'tenant_admin', '', '', '');
INSERT INTO `casbin_rule` VALUES (314003998474310, 'g', '308738821750853', '312939538423877', 'tenant_admin', '', '', '');

SET FOREIGN_KEY_CHECKS = 1;
