package support

import (
	"golang.org/x/crypto/bcrypt"
)

/**
 * check password is correct, return true is correct, else so fail
 * encodePwd: 数据库中加密后的密文
 * pwd: 密码原文
 */
func CheckPassword(encodePwd, pwd string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(encodePwd), []byte(pwd)); err != nil {
		return false
	}
	return true
}

func EncryptPassword(pwd string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.MinCost)
	if err != nil {
		return "", nil
	}
	// 保存在数据库的密码，虽然每次生成都不同，只需保存一份即可
	return string(hash), nil
}
