package tools

import (
	"net"
	"time"

	"github.com/kataras/golog"
)

// get local machine ip
func LocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				golog.Info("local ip=", ipnet.IP.String())
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

const Layout = "2006-01-02 15:04:05"

//const Layout = "2006/01/02 15:04:05"
//const Layout = "2006-01-02"
//const Layout = "15:04:05"

func CurrentTimeString() string {
	return time.Unix(time.Now().Unix(), 0).Format(Layout)
}

func TimeToString(t time.Time) string {
	return t.Format(Layout)
}
