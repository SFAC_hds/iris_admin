package tools

import "fmt"

func LikeAll(v interface{}) string {
	return "%" + fmt.Sprintf("%v", v) + "%"
}
func LikeLeft(v interface{}) string {
	return "%" + fmt.Sprintf("%v", v)
}
func LikeRight(v interface{}) string {
	return fmt.Sprintf("%v", v) + "%"
}
