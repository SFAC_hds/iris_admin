package global

const (
	CodeOk           = 200
	CodeFailure      = 500
	CodeParamsError  = 400
	CodeUnauthorized = 401
	CodeForbidden    = 403
	CodeNotFount     = 404
)
const (
	CodePasswordError Code = iota + 10000
	CodeRecordNotFound
	CodeDataDuplicate
	CodeDuplicate
	CodeUsernameDuplicate
)
const (
	CodeTokenInvalid Code = iota + 20000
	CodeTokenExpired
	CodeTokenEmpty
	CodeUserDisabled
	CodeUserIsRoot
)

// valid_code
const (
	CodeOfValidCodeDuplicate Code = iota + 100000
	CodeExcelFormatError
)

type Code uint

//type Code struct {
//	Code   string `json:"code"`
//	Msg    string `json:"msg"`    // 客户端用
//	Reason string `json:"reason"` // 系统内部用
//}
const commonError = "服务器内部错误。请联系管理员"

// 定义系统内部消息，模拟枚举类型
func (code Code) String() string {
	switch code {
	case CodeOk:
		return "操作成功"
	case CodeFailure:
		return "抱歉，操作失败"
	case CodeParamsError:
		return "抱歉，请求参数错误"
	case CodeUnauthorized:
		return "未登录，或令牌已过期"
	case CodeForbidden:
		return "抱歉，无操作数据的权限"
	case CodeNotFount:
		return "页面不存在"
	case CodeDataDuplicate:
		return "数据重复，请检查"
	case CodeDuplicate:
		return "编码重复，请检查"
	case CodeUsernameDuplicate:
		return "账号重复，请重新输入"
	//
	case CodePasswordError:
		return "抱歉，密码错误！"
	case CodeRecordNotFound:
		return "抱歉，数据不存在"

	case CodeTokenExpired:
		return "抱歉，令牌已过期！"
	case CodeTokenEmpty:
		return "抱歉，令牌不能为空！"
	case CodeUserDisabled:
		return "抱歉，该账号已被禁用/冻结！"
	case CodeUserIsRoot:
		return "抱歉，不能删除超级用户！"

	case CodeOfValidCodeDuplicate:
		return "抱歉，二维码已重复！"
	case CodeExcelFormatError:
		return "抱歉，excel格式错误，请检查！"

	default:
		return commonError
	}
}
