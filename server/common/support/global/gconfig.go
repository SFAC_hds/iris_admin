package global

import (
	"fmt"

	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
	"gopkg.in/yaml.v3"
)

var GConfig *Global

// mysql 通用配置字段
type DBInfo struct {
	Dialect      string `yaml:"dialect"`
	User         string `yaml:"user"`
	Password     string `yaml:"password"`
	Host         string `yaml:"host"`
	Port         int    `yaml:"port"`
	Database     string `yaml:"database"`
	Charset      string `yaml:"charset"`
	ShowSql      bool   `yaml:"showSql"`
	LogLevel     string `yaml:"logLevel"`
	MaxOpenConns int    `yaml:"maxOpenConns"`
	MaxIdleConns int    `yaml:"maxIdleConns"`
	//ParseTime       bool   `yaml:"parseTime"`
	//MaxIdleConns    int    `yaml:"maxIdleConns"`
	//MaxOpenConns    int    `yaml:"maxOpenConns"`
	//ConnMaxLifetime int64  `yaml:"connMaxLifetime: 10"`
	//Sslmode         string `yaml:"sslmode"`
}

// 全局配置文件对应的结构体
type (
	Global struct {
		App *App `yaml:"app"`
		DB  *DB  `yaml:"db"`
	}
	// app config
	App struct {
		iris.Configuration `yaml:"Configuration"`
		Own                struct {
			Port             int      `yaml:"port"`
			LogrusLevel      int      `yaml:"logrus_level"`
			IgnoreURLs       []string `yaml:"ignore_urls,flow"`
			InterceptURLs    []string `yaml:"intercept_urls,flow"`
			JWTTimeout       int      `yaml:"jwt_timeout"`
			Secret           string   `yaml:"secret"`
			WebsocketPool    int      `yaml:"websocket_pool"`
			Tenant           string   `yaml:"tenant"`
			Version          string   `yaml:"version"`
			BookBanner       string   `yaml:"book_banner"`
			ArticleImg       string   `yaml:"article_img"`
			Images           string   `yaml:"images"`
			Template         string   `yaml:"template"`
			LogBatchSize     int      `yaml:"logBatchSize"`
			LogCommitTimeout int      `yaml:"logCommitTimeout"`
		} `yaml: "own"`
	}

	// db
	DB struct {
		Redis struct {
			Addr     string `yaml:"addr"`
			Password string `yaml:"password"`
			DB       int    `yaml:"db"`
			PoolSize int    `yaml:"poolSize"`
		} `yaml:"redis"`
		Mongodb struct {
			Addr           string `yaml:"addr"`
			Database       string `yaml:"database"`
			ConnectTimeout int    `yaml:"connectTimeout"`
			MaxPoolSize    int    `yaml:"maxPoolSize"`
		} `yaml:"mongodb"`
		Mysql struct {
			Db1 DBInfo `yaml:"db1"`
			Db2 DBInfo `yaml:"db2"`
		} `yaml:"mysql"`
	}
)

func (conf Global) DBConnUrl(info DBInfo) string {
	//var info = conf.DB.Mysql
	//"%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=True&loc=Local"
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=True", info.User, info.Password, info.Host, info.Port, info.Database, info.Charset)
	//return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", info.User, info.Password, info.Host, info.Port, info.Database, info.Charset)
}

// 初始化全局配置
func InitGlobalConfig(asset func(name string) ([]byte, error)) {
	var (
		app  = App{}
		db   = DB{}
		data []byte
		err  error
	)

	// app
	data, err = asset("conf/app.yaml")
	if err != nil {
		goto ERR
	}
	if err := yaml.Unmarshal(data, &app); err != nil {
		goto ERR
	}
	golog.Infof("App own config=> %v", app.Own)

	// db
	data, err = asset("conf/db.yaml")
	if err != nil {
		goto ERR
	}
	if err := yaml.Unmarshal(data, &db); err != nil {
		goto ERR
	}
	golog.Infof("DB redis config=> %v", db.Redis)
	golog.Infof("DB mongo config=> %v", db.Mongodb)
	golog.Infof("DB mysql config=> %v", db.Mysql)

	GConfig = &Global{
		App: &app,
		DB:  &db,
	}
	return
ERR:
	golog.Fatalf("~~> 解析全局配置文件错误,原因:%s", err.Error())
}
