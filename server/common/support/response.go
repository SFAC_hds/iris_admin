package support

import (
	"ia/common/support/global"

	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
)

// common error define
func Error(ctx iris.Context, status int, code global.Code) {
	ctx.StatusCode(status)
	ctx.JSON(json(code, nil, true))
}
func Error_(ctx iris.Context, status int, code global.Code, formatMessage string, err error) {
	ctx.StatusCode(status)
	ctx.JSON(json(code, nil, true))
	golog.Errorf(formatMessage, err)
}

// 200 define
func Ok_(ctx iris.Context) {
	Ok(ctx, nil)
}
func Ok(ctx iris.Context, data interface{}) {
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(json(global.CodeOk, data, false))
}
func OkTip(ctx iris.Context, data interface{}) {
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(json(global.CodeOk, data, true))
}

// 403 error define
func Unauthorized(ctx iris.Context, code global.Code) {
	ctx.StatusCode(iris.StatusUnauthorized)
	ctx.JSON(json(code, nil, true))
}

func InternalServerError(ctx iris.Context, code global.Code) {
	ctx.StatusCode(iris.StatusInternalServerError)
	ctx.JSON(json(code, nil, true))
}

/**
 * 统一封装后台管理系统分页表格数据
 * total: 表格数据量
 * columns: 表格列配置
 * result: 表格数据
 * other: 其他数据,用于在一个接口返回.一般是map结构,前端再解析
 */
func VxeTableData[T any](ctx iris.Context, pageSize, currentPage int, total int64, rows T, other interface{}) {
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(json(global.CodeOk, iris.Map{
		"page": iris.Map{
			"pageSize":    pageSize,
			"currentPage": currentPage,
			"total":       total,
		},
		"result": rows,
		"other":  other,
	}, false))
}

// json包装
func json(code global.Code, data interface{}, tip bool) context.Map {
	return iris.Map{
		"code":   code,
		"reason": "",            // 原因,用于系统内部分析,微服务用该字段
		"msg":    code.String(), // 前端用该字段
		"result": data,
		"tip":    tip,
	}
}
