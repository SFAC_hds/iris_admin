package icasbin

import (
	"fmt"
	"ia/apps/admin/assets/conf"
	"ia/common/storage"
	"ia/common/support/global"
	"testing"
)

func Test_InitModel(t *testing.T) {
	global.InitGlobalConfig(conf.Asset)
	storage.InitGDb1()

	InitCasbin(global.GConfig.App.Own.Tenant)
	GCasbin.AddFunction("myFunc", MyFunc)
	var (
		err   error
		ok    bool
		roles []string
	)
	if ok, err = GCasbin.
		Enforce("3", global.GConfig.App.Own.Tenant, "/api/admin/role/grant", "POST", ".*"); err != nil {
		t.Fatal(err)
	}
	// t.Logf("==>%t", ok)
	fmt.Printf("==>%t\n", ok)

	if roles, err = GCasbin.GetImplicitRolesForUser("4", global.GConfig.App.Own.Tenant); err != nil {
		t.Fatal(err)
	}
	for _, v := range roles {
		fmt.Printf("==>%v, %T\n", v, v)
	}

}

func MyFunc(args ...interface{}) (interface{}, error) {
	match := func(key string) bool {
		if key == "/api/admin/user/login" || key == "/api/admin/user/logout" ||
			key == "/api/admin/user/getUserInfo" ||
			key == "/api/admin/menu/list" {
			return true
		}
		return false
	}(args[0].(string))
	return (bool)(match), nil
}
