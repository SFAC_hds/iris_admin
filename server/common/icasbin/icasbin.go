package icasbin

import (
	modeladmin "ia/common/model/admin"
	"ia/common/storage"

	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
	gormadapter "github.com/casbin/gorm-adapter/v3"
	"github.com/kataras/golog"
)

var (
	GCasbin *casbin.Enforcer
)

func InitCasbin(tenant string) {
	var (
		err error
		m   model.Model
		e   *casbin.Enforcer
		// filter = gormadapter.Filter{
		// 	Ptype: []string{"p", "g"},
		// 	V1:    []string{tenant},
		// 	V2:    []string{tenant},
		// }
		adapter *gormadapter.Adapter
	)
	modelStr := `
		[request_definition]
		r = sub, dom, obj, act, suf
		
		[policy_definition]
		p = sub, dom, obj, act, suf
		
		[role_definition]
		g = _, _, _
		
		[policy_effect]
		e = some(where (p.eft == allow))
		
		[matchers]
		m = g(r.sub, p.sub, r.dom) && r.dom == p.dom && keyMatch2(r.obj, p.obj) && regexMatch(r.act, p.act) && r.suf == p.suf || keyMatch(r.obj, "/static*")`
	if tenant == "tenant_admin" {
		modelStr += ` || myFunc(r.obj) || r.sub == "1"` // admin设置id=1为超级用户
	}
	if m, err = model.NewModelFromString(modelStr); err != nil {
		goto ERR
	}

	if adapter, err = gormadapter.NewAdapterByDBWithCustomTable(storage.GDB1, &modeladmin.CasbinRule{}); err != nil {
		goto ERR
	}

	// if e, err = casbin.NewCachedEnforcer(m, adapter); err != nil {
	if e, err = casbin.NewEnforcer(m, adapter); err != nil {
		goto ERR
	}
	e.EnableLog(true)
	// if err = e.LoadFilteredPolicy(filter); err != nil {
	// 	goto ERR
	// }
	GCasbin = e
	return
ERR:
	golog.Fatalf("~~> Casbin策略加载失败,原因:%s", err.Error())
}
