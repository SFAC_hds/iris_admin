package admin

import (
	"ia/common/model"
)

// 后台用户
type AdminUser struct {
	model.Model
	Username string `json:"username" gorm:"not null;default:'';unique;" xlsx:"账号"`
	Password string `json:"password" gorm:"not null;default:'';"`
	Name     string `json:"name" gorm:"not null;default:'';"`
	Gender   int8   `json:"gender" gorm:"not null;default:0;"` // 1=男 2=女 0=未知
	Enable   int8   `json:"enable" gorm:"not null;default:1;"`
	Age      int    `json:"age" xlsx:"年龄"`
	Phone    string `json:"phone" gorm:"not null;default:'';unique;"`
	Email    string `json:"email" gorm:"not null;default:'';"`
	Avatar   string `json:"userface" gorm:"not null;default:'';"`
	Memo     string `json:"memo" gorm:"not null;default:'';"` // 备注
	OrgId    int64  `json:"orgId" gorm:"not null;default:0;"`
	Online   bool   `json:"online" gorm:"-"` // 是否在线
}

// func (m *AdminUser) BeforeCreate(tx *gorm.DB) error {
// 	m.CreatedTime = tools.CurrentTimeString()
// 	return nil
// }
