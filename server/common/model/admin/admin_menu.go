package admin

type AdminMenu struct {
	Id        uint   `json:"id" gorm:"primaryKey"`
	Path      string `json:"path" gorm:"not null;type:string;"`
	Name      string `json:"name" gorm:"not null;default:'';"`
	Component string `json:"component" gorm:"not null;default:'';"`
	Redirect  string `json:"redirect" gorm:"not null;default:'';"`
	MetaId    uint   `json:"metaId"`
	ParentId  uint   `json:"parentId" gorm:"not null;default:0;"`
	Sort      int    `json:"sort" gorm:"not null;default:0;"`
}

type AdminMenuMeta struct {
	Id      uint `json:"id" gorm:"primaryKey"`
	OrderNo int  `json:"orderNo" gorm:"not null;default:0;"`
	// title
	Title string `json:"title" gorm:"not null;default:'';"`
	// dynamic router level.
	DynamicLevel int `json:"dynamicLevel" gorm:"not null;default:0;"`
	// dynamic router real route path (For performance).
	RealPath string `json:"realPath" gorm:"not null;default:'';"`
	// Whether to ignore permissions
	IgnoreAuth bool `json:"ignoreAuth" gorm:"not null;default:0;"`
	// role info
	// roles?: RoleEnum[];
	// Whether not to cache
	IgnoreKeepAlive bool `json:"ignoreKeepAlive" gorm:"not null;default:0;"`
	// Is it fixed on tab
	Affix bool `json:"affix" gorm:"not null;default:0;"`
	// icon on tab
	Icon     string `json:"icon" gorm:"not null;default:'';"`
	FrameSrc string `json:"frameSrc" gorm:"not null;default:'';"`
	// current page transition
	TransitionName string `json:"transitionName" gorm:"not null;default:'';"`
	// Whether the route has been dynamically added
	HideBreadcrumb bool `json:"hideBreadcrumb" gorm:"not null;default:0;"`
	// Hide submenu
	HideChildrenInMenu bool `json:"hideChildrenInMenu" gorm:"not null;default:0;"`
	// Carrying parameters
	CarryParam bool `json:"carryParam" gorm:"not null;default:0;"`
	// Used internally to mark single-level menus
	Single bool `json:"single" gorm:"not null;default:0;"`
	// Currently active menu
	CurrentActiveMenu string `json:"currentActiveMenu" gorm:"not null;default:'';"`
	// Never show in tab
	HideTab bool `json:"hideTab" gorm:"not null;default:0;"`
	// Never show in menu
	HideMenu bool `json:"hideMenu" gorm:"not null;default:0;"`
	IsLink   bool `json:"isLink" gorm:"not null;default:0;"`
	// only build for Menu
	IgnoreRoute bool `json:"ignoreRoute" gorm:"not null;default:0;"`
	// Hide path for children
	HidePathForChildren bool `json:"hidePathForChildren" gorm:"not null;default:0;"`
}
