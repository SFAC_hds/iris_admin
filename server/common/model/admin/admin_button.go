package admin

import (
	"ia/common/model"
)

type AdminButton struct {
	Group      string `json:"group" gorm:"not null;default:'';comment:按钮组"`
	Name       string `json:"name" gorm:"not null;default:'';comment:按钮名称"`
	Icon       string `json:"icon" gorm:"not null;default:'';comment:图标;"`
	Func       string `json:"func" gorm:"not null;default:'';comment:方法名;"`
	ParentId   uint   `json:"parentId" gorm:"not null;comment:父ID;"`
	Place      int    `json:"place" gorm:"not null;default:0;comment:按钮的位置;"`
	Sort       int    `json:"sort" gorm:"not null;default:0;"`
	Enable     int8   `json:"enable" gorm:"not null;default:1;"`
	Disable    int8   `json:"disable" gorm:"not null;default:0;comment:0=不禁用 1=没数量 2=有数量;"`
	Tip        int8   `json:"tip" gorm:"not null;default:0;comment:是否提示"`
	TipContent string `json:"tipContent" gorm:"not null;default:'';comment:提示内容"`
	Memo       string `json:"memo" gorm:"not null;default:'';"`
	model.Model
}
