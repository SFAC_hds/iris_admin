package admin

import "ia/common/model"

type AdminRole struct {
	model.Model
	Name string `json:"name" gorm:"not null;"`
	Memo string `json:"memo" gorm:"not null;default:'';"`
}

type AdminRoleMenu struct {
	Id  int64 `json:"id" gorm:"primaryKey"`
	Rid int64 `json:"rid" gorm:"not null"`
	Mid int64 `json:"mid" gorm:"not null"`
	Bid int64 `json:"bid" gorm:"not null;"`
}
