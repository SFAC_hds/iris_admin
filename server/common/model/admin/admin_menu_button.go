package admin

// 菜单的全部按钮
type AdminMenuButton struct {
	Id  uint `json:"id" gorm:"primaryKey"`
	Mid uint `json:"mid" gorm:"not null"`
	Bid uint `json:"bid" gorm:"not null"`
}
