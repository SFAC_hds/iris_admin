package admin

import (
	"ia/common/model"
)

type AdminOrg struct {
	model.Model
	Code            string `json:"code" gorm:"not null;default:'';unique;"`
	Name            string `json:"name" gorm:"not null;default:'';"`
	Enable          int8   `json:"enable" gorm:"not null;default:1;"`
	Area            string `json:"area" gorm:"not null;default:'';"`
	OfficeAddress   string `json:"officeAddress" gorm:"not null;default:'';"`
	RegisterAddress string `json:"registerAddress" gorm:"not null;default:'';"`
	Person          string `json:"person" gorm:"not null;default:'';"`
	Phone           string `json:"phone" gorm:"not null;default:'';"`
	Memo            string `json:"memo" gorm:"not null;default:'';"`
}
