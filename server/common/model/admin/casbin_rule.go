package admin

type CasbinRule struct {
	Id    uint   `json:"id" gorm:"primaryKey;autoIncrement"`
	Ptype string `json:"ptype" gorm:"not null;type:string;size:20;"`
	V0    string `json:"v0" gorm:"not null;type:string;size:100;"`
	V1    string `json:"v1" gorm:"not null;type:string;size:100;"`
	V2    string `json:"v2" gorm:"not null;type:string;size:100;"`
	V3    string `json:"v3" gorm:"not null;type:string;size:100;"`
	V4    string `json:"v4" gorm:"not null;type:string;size:100;"`
	V5    string `json:"v5" gorm:"not null;type:string;default:'';size:100;"`
}
