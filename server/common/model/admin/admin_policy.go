package admin

import "ia/common/model"

type AdminPolicy struct {
	model.Model
	GroupName string `json:"groupName" gorm:"not null;"`
	Name      string `json:"name" gorm:"not null;"`
	Memo      string `json:"memo" gorm:"not null;default:'';"`
	IsSystem  bool   `json:"isSystem" gorm:"not null;default:1;"`
	Logable   bool   `json:"logable" gorm:"not null;default:0;"`
	//
	V1 string `json:"v1" gorm:"not null;type:string;size:100;"`
	V2 string `json:"v2" gorm:"not null;type:string;size:100;"`
	V3 string `json:"v3" gorm:"not null;type:string;size:100;"`
	V4 string `json:"v4" gorm:"not null;type:string;size:100;"`
	V5 string `json:"v5" gorm:"not null;type:string;default:'';size:100;"`
}

type AdminRolePolicy struct {
	Id  uint `json:"id" gorm:"primaryKey"`
	Rid uint `json:"rid" gorm:"not null"`
	Pid uint `json:"pid" gorm:"not null"`
}
