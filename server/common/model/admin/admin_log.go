package admin

import (
	"ia/common/model"
)

type AdminLog struct {
	model.Model
	Path           string `json:"path" gorm:"not null;"`
	Method         string `json:"method" gorm:"not null;"`
	IpAddr         string `json:"ipAddr" gorm:"not null;default:'';"`
	City           string `json:"city" gorm:"not null;default:'';"`
	Os             string `json:"os" gorm:"not null;default:''"`
	UserAgent      string `json:"userAgent" gorm:"not null;default:'';"`
	Params         string `json:"params" gorm:"not null;default:'';"`
	Data           string `json:"data" gorm:"not null;default:'';"`
	GroupName      string `json:"groupName" gorm:"not null;default:'';"` // 接口组
	Name           string `json:"name" gorm:"not null;default:'';"`      // 接口名
	Duration       int    `json:"duration" gorm:"not null;default:0;"`   // 耗时:毫秒,1/1000秒
	RespStatusCode int    `json:"respStatusCode" gorm:"not null;default:0;"`
}
