package model

// 通用对象模型数据,不用原生的grom.Model,太多不可思议的事情,禁用!
// 将gorm.Model的time替换为string
type Model struct {
	Id          int64   `json:"id" gorm:"primaryKey"`
	CreatedTime string  `json:"createdTime" gorm:"size:20;type:string;" xlsx:"创建时间"`
	CreatorId   int64   `json:"creatorId" gorm:"comment:'创建者ID'"`
	UpdatedTime string  `json:"updatedTime" gorm:"size:20" xlsx:"更新时间"`
	DeletedTime *string `json:"deletedTime" gorm:"index;size:20" xlsx:"删除时间"`
	DeleterId   int64   `json:"deleterId" gorm:"comment:'删除者ID'"`
}
