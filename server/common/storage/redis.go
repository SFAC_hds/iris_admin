package storage

import (
	"fmt"
	"ia/common/support/global"
	"time"

	"github.com/go-redis/redis"
	"github.com/kataras/golog"
)

var G_Redis *Redis

type Redis struct {
	client *redis.Client
}

func (r *Redis) SetToken(format, id string, token string) (err error) {
	err = r.client.Set(fmt.Sprintf(format, id), token,
		time.Duration(global.GConfig.App.Own.JWTTimeout)*time.Minute).Err()
	return
}

func (r *Redis) GetToken(format, id string) (token string, err error) {
	token, err = r.client.Get(fmt.Sprintf(format, id)).Result()
	return
}

func (r *Redis) DelToken(format, id string) (result int64, err error) {
	result, err = r.client.Del(fmt.Sprintf(format, id)).Result()
	return
}

// Init
func InitRedis() {
	client := redis.NewClient(&redis.Options{
		Addr:     global.GConfig.DB.Redis.Addr,
		Password: global.GConfig.DB.Redis.Password,
		DB:       global.GConfig.DB.Redis.DB, // 连接的库位
		PoolSize: global.GConfig.DB.Redis.PoolSize,
	})
	if _, err := client.Ping().Result(); err != nil {
		golog.Fatalf("~~> 启动失败! Redis未建立连接,原因:%s", err.Error())
	}
	G_Redis = &Redis{
		client: client,
	}
}
