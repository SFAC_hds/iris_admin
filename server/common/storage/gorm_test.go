package storage

import (
	"crypto/md5"
	"fmt"
	"ia/apps/admin/assets/conf"
	ooutadmin "ia/apps/admin/dto/oout/admin"
	"ia/common/model"
	modeladmin "ia/common/model/admin"
	"ia/common/support"
	"ia/common/support/global"
	"strings"
	"testing"

	"gorm.io/gorm"
)

func Test_1(t *testing.T) {
	global.InitGlobalConfig(conf.Asset)
	InitGDb1()

	err := GDB1.AutoMigrate(
		&modeladmin.AdminUser{}, &modeladmin.AdminOrg{},
		&modeladmin.CasbinRule{}, &modeladmin.AdminPolicy{}, &modeladmin.AdminRolePolicy{},
		&modeladmin.AdminMenu{}, &modeladmin.AdminMenuMeta{}, &modeladmin.AdminMenuButton{},
		&modeladmin.AdminRole{}, &modeladmin.AdminRoleMenu{},
		&modeladmin.AdminLog{}, &modeladmin.AdminButton{},
	)

	if err != nil {
		t.Fatal(err)
	}
}

// 初始化系统数据
func Test_InitSystem(t *testing.T) {
	global.InitGlobalConfig(conf.Asset)
	InitGDb1()
	var (
		err        error
		enPassword string
		user       = modeladmin.AdminUser{
			Username: "root", Password: enPassword, Name: "超级用户", Gender: 2, Enable: 1, Phone: "15837284339",
		}
		uid = user.Id
		//
		buttonList = [...]modeladmin.AdminButton{
			{Model: model.Model{CreatorId: uid}, Name: "新建", Place: 0, Enable: 1, Func: "create"},
			{Model: model.Model{CreatorId: uid}, Name: "编辑", Place: 2, Enable: 1, Func: "edit"},
			{Model: model.Model{CreatorId: uid}, Name: "删除", Place: 0, Enable: 1, Func: "batchDelete", Tip: 1, TipContent: "警告：数据删除后不可恢复！确定删除吗？"},
			{Model: model.Model{CreatorId: uid}, Name: "复制行", Place: 2, Enable: 1, Func: "copy", Tip: 1, TipContent: "提示：确定复制该行数据吗？"},
			{Model: model.Model{CreatorId: uid}, Name: "授权", Place: 2, Enable: 1, Func: "grant"},
			//
			{Model: model.Model{CreatorId: uid}, Name: "刷新表", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "导入", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "导出", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "打印", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "最大化", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "自定义列", Place: 1, Enable: 1},
		}
		// menuList = [...]modeladmin.AdminMenu{
		// 	{Model: model.Model{CreatorId: uid}, Path: "button", Name: "Button", Component: "/sys_setting/sys/button/index"},
		// }
	)

	if enPassword, err = support.EncryptPassword("123456"); err != nil {
		goto ERR
	}

	// 清空所有admin的表
	if err = clears(); err != nil {
		goto ERR
	}

	if err = GDB1.Transaction(func(tx *gorm.DB) error {
		// user
		user.Password = enPassword
		if err := tx.Create(&user).Error; err != nil {
			return err
		}
		if err := tx.Model(&modeladmin.AdminUser{}).Where("id=?", user.Id).Update("id", 1).Error; err != nil {
			return err
		}
		// button
		if err = tx.CreateInBatches(&buttonList, 100).Error; err != nil {
			return err
		}
		// menu
		return nil
	}); err != nil {
		goto ERR
	}
	return
ERR:
	fmt.Println(err)
	t.Fatal(err)
}

func clears() error {
	return GDB1.Transaction(func(tx *gorm.DB) error {
		var err error
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminUser{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.CasbinRule{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminPolicy{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminRolePolicy{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminMenu{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminMenuMeta{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminMenuButton{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminRole{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminRoleMenu{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminLog{}).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminButton{}).Error; err != nil {
			return err
		}
		return err
	})
}

func Test_c(t *testing.T) {
	sum := md5.Sum([]byte("123456"))
	t.Logf("%x", sum)
	t.Logf("%v", "e10adc3949ba59abbe56e057f20f883e")

	t.Logf("%t", strings.EqualFold("e10adc3949ba59abbe56e057f20f883e", fmt.Sprintf("%x", sum)))
}

func Test_copy(t *testing.T) {
	global.InitGlobalConfig(conf.Asset)
	InitGDb1()

	var userco ooutadmin.AdminUserOut
	var err error
	columnTypes := make([]gorm.ColumnType, 0)
	if columnTypes, err = GDB1.Migrator().ColumnTypes(&userco); err != nil {
		t.Fatal(err)
	}
	for _, v := range columnTypes {
		a, _ := v.Comment()
		fmt.Printf("%v, %v\n", v.Name(), a)
	}

	userList := make([]*ooutadmin.AdminUserOut, 0)

	if err := GDB1.Raw("select * from admin_user").
		Omit("password").Scan(&userList).Error; err != nil {
		t.Fatal(err)
	}

	for _, v := range userList {
		fmt.Printf("%v\n", v)
	}
}

func Test_adminRole(t *testing.T) {
	global.InitGlobalConfig(conf.Asset)
	InitGDb1()

	// m := query.Use(GDB1).AdminMenuOut
	// list, _ := m.WithContext(context.TODO()).Preload(field.Associations).Find()
	// for _, v := range list {
	// 	fmt.Printf("%v, %v\n", v, v.Meta)
	// }
}
