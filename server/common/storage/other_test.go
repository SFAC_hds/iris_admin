package storage

import (
	"fmt"
	"ia/common/support"
	"sync"
	"testing"
	"time"

	"github.com/yitter/idgenerator-go/idgen"
)

func Test_o1(t *testing.T) {
	var a = "2006-01-02 15:04:05"
	var fmt1 = "2006/01/02 15:04:05"
	var fmt2 = "2006-01-02"
	var fmt3 = "15:04:05"

	t.Log(time.Unix(time.Now().Unix(), 0).Format(a))
	t.Log(time.Unix(time.Now().Unix(), 0).Format(fmt1))
	t.Log(time.Unix(time.Now().Unix(), 0).Format(fmt2))
	t.Log(time.Unix(time.Now().Unix(), 0).Format(fmt3))
}

func Test_o2(t *testing.T) {
	pwd, _ := support.EncryptPassword("123456")

	fmt.Println(pwd)
}

var wg sync.WaitGroup

func Test_o3(t *testing.T) {
	wg.Add(3)
	go a()
	go b()
	go c()
	wg.Wait()
}
func a() {
	for i := 0; i < 1000; i++ {
		a := idgen.NextId()
		fmt.Println(a)
	}
	defer wg.Done()
}
func b() {
	for i := 0; i < 1000; i++ {
		a := idgen.NextId()
		fmt.Println(a)
	}
	defer wg.Done()
}
func c() {
	for i := 0; i < 1000; i++ {
		a := idgen.NextId()
		fmt.Println(a)
	}
	defer wg.Done()
}
