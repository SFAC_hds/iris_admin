package auth

import (
	"ia/common/icasbin"
	"ia/common/support/global"
)

func InitAuth() {
	icasbin.InitCasbin(global.GConfig.App.Own.Tenant)
	icasbin.GCasbin.AddFunction("myFunc", MyFunc)
}
func MyFunc(args ...interface{}) (interface{}, error) {
	match := func(key string) bool {
		if key == "/api/admin/user/login" || key == "/api/admin/user/logout" ||
			key == "/api/admin/user/getUserInfo" || key == "/api/admin/user/own/reset_info" || key == "/api/admin/user/own/reset_password" ||
			key == "/api/admin/menu/ownList" {
			return true
		}
		return false
	}(args[0].(string))
	return (bool)(match), nil
}
