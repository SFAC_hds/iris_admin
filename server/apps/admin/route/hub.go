package route

import (
	"ia/apps/admin/bootstrap"
	routeadmin "ia/apps/admin/route/admin"
	serviceadmin "ia/apps/admin/service/admin"

	"github.com/kataras/iris/v12"
)

func Hub(b *bootstrap.Bootstrapper) {
	api := b.Api

	api.Party("/user").ConfigureContainer(func(r *iris.APIContainer) {
		r.RegisterDependency(serviceadmin.NewAdminlUserService())
		route := routeadmin.NewAdminUserRoute()
		r.Post("/login", route.Login).Describe("").Title = "用户~login~"
		r.Get("/getUserInfo", route.GetUserInfo).Describe("").Title = "用户~获取用户信息~"
		r.Get("/logout", route.Logout).Describe("").Title = "用户~logout~"
		r.Post("/resetPassword", route.ResetPassword).Describe("").Title = "用户~重置密码~"
		//
		r.Post("/", route.Create).Describe("").Title = "用户~创建~"
		r.Delete("/", route.Delete).Describe("").Title = "用户~删除~"
		r.Put("/", route.Edit).Describe("").Title = "用户~修改~"
		r.Get("/table", route.Table).Describe("").Title = "用户~管理报表~"
		// r.Get("/list", hero.Handler(impl.List)).Describe("").Title = "用户~列表~"
		//
		// adminUser.Put("/own/reset_info", hero.Handler(aus.ResetOwnInfo)).Describe("用户管理接口~自己修改自己信息~")
		// adminUser.Put("/own/reset_password", hero.Handler(aus.ResetOwnPassword)).Describe("用户管理接口~自己重置密码~")
	})

	api.Party("/menu").ConfigureContainer(func(r *iris.APIContainer) {
		r.RegisterDependency(serviceadmin.NewAdminMenuService())
		route := routeadmin.NewAdminMenuRoute()
		r.Post("/", route.Create).Describe("").Title = "菜单~创建~"
		r.Delete("/", route.Delete).Describe("").Title = "菜单~删除~"
		r.Put("/", route.Edit).Describe("").Title = "菜单~修改~"
		r.Get("/list", route.List).Describe("").Title = "菜单~列表~"
		r.Get("/ownList", route.OwnList).Describe("").Title = "菜单~拥有的列表~"
	})
	api.Party("/button").ConfigureContainer(func(r *iris.APIContainer) {
		r.RegisterDependency(serviceadmin.NewAdminButtonService())
		route := routeadmin.NewAdminButtonRoute()
		r.Post("/", route.Create).Describe("").Title = "按钮~创建~"
		r.Post("/generate", route.Generate).Describe("").Title = "按钮~生成按钮~"
		r.Put("/", route.Edit).Describe("").Title = "按钮~修改~"
		r.Delete("/", route.Delete).Describe("").Title = "按钮~删除~"
		r.Get("/list", route.List).Describe("").Title = "按钮~列表~"
	})

	api.Party("/role").ConfigureContainer(func(r *iris.APIContainer) {
		r.RegisterDependency(serviceadmin.NewAdminRoleService())
		route := routeadmin.NewAdminRoleRoute()
		r.Post("/", route.Create).Describe("").Title = "角色~创建~"
		r.Delete("/", route.Delete).Describe("").Title = "角色~删除~"
		r.Put("/", route.Edit).Describe("").Title = "角色~修改~"
		r.Get("/table", route.Table).Describe("").Title = "角色~管理报表~"
		r.Get("/list", route.List).Describe("").Title = "角色~列表~"
		r.Post("/grant", route.Grant).Describe("").Title = "角色~授权~"
		r.Get("/grant/owned", route.Owned).Describe("").Title = "角色~已有的权限~"
	})

	api.Party("/policy").ConfigureContainer(func(r *iris.APIContainer) {
		r.RegisterDependency(serviceadmin.NewAdminPolicyService())
		route := routeadmin.NewAdminPolicyRoute()
		r.Delete("/", route.Delete).Describe("").Title = "接口策略~删除~"
		r.Get("/table", route.Table).Describe("").Title = "接口策略~管理报表~"
		r.Get("/reloadPolicy", route.ReloadPolicy).Describe("").Title = "接口策略~重载策略~"
		r.Get("/list", route.List).Describe("").Title = "接口策略~列表~"
		r.Put("/log", route.Log).Describe("").Title = "接口策略~日志埋点~"
	})

	api.Party("/org").ConfigureContainer(func(r *iris.APIContainer) {
		r.RegisterDependency(serviceadmin.NewAdminOrgService())
		route := routeadmin.NewAdminOrgRoute()
		r.Post("/", route.Create).Describe("").Title = "组织机构~创建~"
		r.Delete("/", route.Delete).Describe("").Title = "组织机构~删除~"
		r.Put("/", route.Edit).Describe("").Title = "组织机构~修改~"
		r.Get("/table", route.Table).Describe("").Title = "组织机构~管理报表~"
		r.Get("/list", route.List).Describe("").Title = "组织机构~列表~"
	})

	api.Party("/log").ConfigureContainer(func(r *iris.APIContainer) {
		r.RegisterDependency(serviceadmin.NewAdminLogService())
		route := routeadmin.NewAdminLogRoute()
		r.Delete("/", route.Delete).Describe("").Title = "操作日志~删除~"
		r.Get("/table", route.Table).Describe("").Title = "操作日志~管理报表~"
	})
}
