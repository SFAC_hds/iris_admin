package admin

import (
	serviceadmin "ia/apps/admin/service/admin"
	modeladmin "ia/common/model/admin"
	"ia/common/support"
	"ia/common/support/global"

	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
)

func NewAdminButtonRoute() *adminButtonRoute {
	return &adminButtonRoute{}
}

type adminButtonRoute struct{}

func (r *adminButtonRoute) Create(ctx iris.Context, service serviceadmin.AdminButtonService) {
	var (
		code global.Code
		err  error
		in   modeladmin.AdminButton
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}
	in.CreatorId = ctx.Values().GetInt64Default(global.Uid, 0)
	if err = service.Create(in); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminButtonRoute) Generate(ctx iris.Context, service serviceadmin.AdminButtonService) {
	var (
		code global.Code
		err  error
		uid  = ctx.Values().GetInt64Default(global.Uid, 0)
	)
	if err = service.Generate(uid); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminButtonRoute) Edit(ctx iris.Context, service serviceadmin.AdminButtonService) {
	var (
		code global.Code
		err  error
		in   modeladmin.AdminButton
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}
	if err = service.Edit(in); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminButtonRoute) Delete(ctx iris.Context, service serviceadmin.AdminButtonService) {
	var (
		code   global.Code
		err    error
		idList = make([]int64, 0)
	)
	if err = ctx.ReadJSON(&idList); err != nil {
		goto ERR
	}
	if err = service.DeleteByIdList(idList); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminButtonRoute) List(ctx iris.Context, service serviceadmin.AdminButtonService) {
	var (
		code global.Code
		err  error
		rows = make([]*modeladmin.AdminButton, 0)
	)
	if rows, err = service.GetList(); err != nil {
		goto ERR
	}
	support.Ok(ctx, rows)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}
