package admin

import (
	"ia/apps/admin/dto/oin"
	oinadmin "ia/apps/admin/dto/oin/admin"
	ooutadmin "ia/apps/admin/dto/oout/admin"
	serviceadmin "ia/apps/admin/service/admin"
	modeladmin "ia/common/model/admin"
	"ia/common/support"
	"ia/common/support/global"

	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
)

func NewAdminPolicyRoute() *adminPolicyRoute {
	return &adminPolicyRoute{}
}

type adminPolicyRoute struct{}

func (r *adminPolicyRoute) Delete(ctx iris.Context, service serviceadmin.AdminPolicyService) {
	var (
		code   global.Code
		err    error
		idList = make([]int64, 0)
	)
	if err = ctx.ReadJSON(&idList); err != nil {
		goto ERR
	}
	if err = service.DeleteByIdList(idList); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminPolicyRoute) Table(ctx iris.Context, service serviceadmin.AdminPolicyService) {
	var (
		code  global.Code
		err   error
		in    oin.VxeTableIn
		total int64
		rows  = make([]*modeladmin.AdminPolicy, 0)
	)
	if err = ctx.ReadQuery(&in); err != nil {
		goto ERR
	}
	if rows, total, err = service.GetTable(in); err != nil {
		goto ERR
	}
	support.VxeTableData(ctx, in.PageSize, in.CurrentPage, total, rows, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminPolicyRoute) List(ctx iris.Context, service serviceadmin.AdminPolicyService) {
	var (
		code global.Code
		err  error
		rows = make([]*ooutadmin.AdminPolicyOut, 0)
	)
	if rows, err = service.GetList(); err != nil {
		goto ERR
	}
	support.Ok(ctx, rows)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminPolicyRoute) ReloadPolicy(ctx iris.Context, service serviceadmin.AdminPolicyService) {
	var (
		err  error
		code global.Code
	)
	if err = service.ReloadPolicy(); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminPolicyRoute) Log(ctx iris.Context, service serviceadmin.AdminPolicyService) {
	var (
		code global.Code
		err  error
		in   oinadmin.AdminPolicyOfLogIn
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}
	if err = service.Log(in); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}
