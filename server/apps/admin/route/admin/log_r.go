package admin

import (
	oin "ia/apps/admin/dto/oin/admin"
	oout "ia/apps/admin/dto/oout/admin"
	"ia/apps/admin/repository/query"
	serviceadmin "ia/apps/admin/service/admin"
	"ia/common/storage"
	"ia/common/support"
	"ia/common/support/global"

	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
)

func NewAdminLogRoute() *adminLogRoute {
	return &adminLogRoute{}
}

type adminLogRoute struct{}

func (r *adminLogRoute) Delete(ctx iris.Context, service serviceadmin.AdminLogService) {
	var (
		code   global.Code
		err    error
		idList = make([]int64, 0)
	)
	if err = ctx.ReadJSON(&idList); err != nil {
		goto ERR
	}
	if err = service.DeleteByIdList(idList); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminLogRoute) Table(ctx iris.Context, service serviceadmin.AdminLogService) {
	var (
		code  global.Code
		err   error
		in    oin.LogIn
		total int64
		rows  = make([]*oout.AdminLogOut, 0)

		db         = query.Use(storage.GDB1)
		userList   = make([]*oout.AdminUserOut, 0)
		policyList = make([]*oout.AdminPolicyOut, 0)
	)
	if err = ctx.ReadQuery(&in); err != nil {
		goto ERR
	}
	if rows, total, err = service.GetTable(in); err != nil {
		goto ERR
	}

	if userList, err = db.AdminUserOut.Find(); err != nil {
		goto ERR
	}
	if policyList, err = db.AdminPolicyOut.Find(); err != nil {
		goto ERR
	}
	support.VxeTableData(ctx, in.PageSize, in.CurrentPage, total, rows, iris.Map{
		"userList":   userList,
		"policyList": policyList,
	})
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}
