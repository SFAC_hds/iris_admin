package admin

import (
	oin "ia/apps/admin/dto/oin/admin"
	oout "ia/apps/admin/dto/oout/admin"
	serviceadmin "ia/apps/admin/service/admin"
	"ia/common/support"
	"ia/common/support/global"

	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
)

func NewAdminMenuRoute() *adminMenuRoute {
	return &adminMenuRoute{}
}

type adminMenuRoute struct{}

func (r *adminMenuRoute) Create(ctx iris.Context, service serviceadmin.AdminMenuService) {
	var (
		code global.Code
		err  error
		in   oin.AdminMenuIn
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}
	if err = service.Create(in); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminMenuRoute) Delete(ctx iris.Context, service serviceadmin.AdminMenuService) {
	var (
		code   global.Code
		err    error
		idList = make([]int64, 0)
	)
	if err = ctx.ReadJSON(&idList); err != nil {
		goto ERR
	}
	if err = service.DeleteByIdList(idList); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminMenuRoute) Edit(ctx iris.Context, service serviceadmin.AdminMenuService) {
	var (
		code global.Code
		err  error
		in   oin.AdminMenuIn
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}

	if err = service.Edit(in); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminMenuRoute) List(ctx iris.Context, service serviceadmin.AdminMenuService) {
	var (
		code global.Code
		err  error
		rid  = ctx.URLParamInt64Default("rid", 0)
		rows = make([]*oout.AdminMenuOut, 0)
	)
	if rows, err = service.GetList(rid); err != nil {
		goto ERR
	}
	support.Ok(ctx, rows)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminMenuRoute) OwnList(ctx iris.Context, service serviceadmin.AdminMenuService) {
	var (
		code global.Code
		err  error
		uid  = ctx.Values().GetInt64Default(global.Uid, 0)
		rows = make([]*oout.AdminMenuOut, 0)
	)
	if rows, err = service.GetOwnList(uid); err != nil {
		goto ERR
	}
	support.Ok(ctx, rows)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}
