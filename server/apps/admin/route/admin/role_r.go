package admin

import (
	"ia/apps/admin/dto/oin"
	oinadmin "ia/apps/admin/dto/oin/admin"
	serviceadmin "ia/apps/admin/service/admin"
	modeladmin "ia/common/model/admin"
	"ia/common/support"
	"ia/common/support/global"

	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
)

func NewAdminRoleRoute() *adminRoleRoute {
	return &adminRoleRoute{}
}

type adminRoleRoute struct{}

func (r *adminRoleRoute) Create(ctx iris.Context, service serviceadmin.AdminRoleService) {
	var (
		code global.Code
		err  error
		in   modeladmin.AdminRole
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}
	if err = service.Create(in); err != nil {
		goto ERR
	}
	support.Ok_(ctx)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminRoleRoute) Edit(ctx iris.Context, service serviceadmin.AdminRoleService) {
	var (
		code global.Code
		err  error
		in   modeladmin.AdminRole
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}
	if err = service.Edit(in); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminRoleRoute) Delete(ctx iris.Context, service serviceadmin.AdminRoleService) {
	var (
		code   global.Code
		err    error
		idList = make([]int64, 0)
	)
	if err = ctx.ReadJSON(&idList); err != nil {
		goto ERR
	}
	if err = service.DeleteByIdList(idList); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminRoleRoute) Table(ctx iris.Context, service serviceadmin.AdminRoleService) {
	var (
		code  global.Code
		err   error
		in    oin.VxeTableIn
		total int64
		rows  = make([]*modeladmin.AdminRole, 0)
	)
	if err = ctx.ReadQuery(&in); err != nil {
		goto ERR
	}
	if rows, total, err = service.GetTable(in); err != nil {
		goto ERR
	}
	support.VxeTableData(ctx, in.PageSize, in.CurrentPage, total, rows, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminRoleRoute) List(ctx iris.Context, service serviceadmin.AdminRoleService) {
	var (
		code global.Code
		err  error
		rows = make([]*modeladmin.AdminRole, 0)
	)
	if rows, err = service.GetList(); err != nil {
		goto ERR
	}
	support.Ok(ctx, rows)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminRoleRoute) Grant(ctx iris.Context, service serviceadmin.AdminRoleService) {
	var (
		code global.Code
		err  error
		in   oinadmin.AdminRoleGrant
	)
	if err = ctx.ReadJSON(&in); err != nil {
		goto ERR
	}
	if err = service.Grant(in); err != nil {
		goto ERR
	}
	support.OkTip(ctx, nil)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}

func (r *adminRoleRoute) Owned(ctx iris.Context, service serviceadmin.AdminRoleService) {
	var (
		code   global.Code
		err    error
		rid    = ctx.URLParamInt64Default("rid", 0)
		result iris.Map
	)
	if result, err = service.Owned(rid); err != nil {
		goto ERR
	}
	support.Ok(ctx, result)
	return
ERR:
	golog.Errorf("错误：%s", err)
	support.InternalServerError(ctx, code)
}
