// Code generated by gorm.io/gen. DO NOT EDIT.
// Code generated by gorm.io/gen. DO NOT EDIT.
// Code generated by gorm.io/gen. DO NOT EDIT.

package query

import (
	"context"
	"database/sql"

	"gorm.io/gorm"
)

func Use(db *gorm.DB) *Query {
	return &Query{
		db:                 db,
		AdminButtonOut:     newAdminButtonOut(db),
		AdminCasbinRuleOut: newAdminCasbinRuleOut(db),
		AdminLogOut:        newAdminLogOut(db),
		AdminMenuOut:       newAdminMenuOut(db),
		AdminOrgOut:        newAdminOrgOut(db),
		AdminPolicyOut:     newAdminPolicyOut(db),
		AdminRoleOut:       newAdminRoleOut(db),
		AdminRolePolicy:    newAdminRolePolicy(db),
		AdminUserOut:       newAdminUserOut(db),
	}
}

type Query struct {
	db *gorm.DB

	AdminButtonOut     adminButtonOut
	AdminCasbinRuleOut adminCasbinRuleOut
	AdminLogOut        adminLogOut
	AdminMenuOut       adminMenuOut
	AdminOrgOut        adminOrgOut
	AdminPolicyOut     adminPolicyOut
	AdminRoleOut       adminRoleOut
	AdminRolePolicy    adminRolePolicy
	AdminUserOut       adminUserOut
}

func (q *Query) Available() bool { return q.db != nil }

func (q *Query) clone(db *gorm.DB) *Query {
	return &Query{
		db:                 db,
		AdminButtonOut:     q.AdminButtonOut.clone(db),
		AdminCasbinRuleOut: q.AdminCasbinRuleOut.clone(db),
		AdminLogOut:        q.AdminLogOut.clone(db),
		AdminMenuOut:       q.AdminMenuOut.clone(db),
		AdminOrgOut:        q.AdminOrgOut.clone(db),
		AdminPolicyOut:     q.AdminPolicyOut.clone(db),
		AdminRoleOut:       q.AdminRoleOut.clone(db),
		AdminRolePolicy:    q.AdminRolePolicy.clone(db),
		AdminUserOut:       q.AdminUserOut.clone(db),
	}
}

type queryCtx struct {
	AdminButtonOut     *adminButtonOutDo
	AdminCasbinRuleOut *adminCasbinRuleOutDo
	AdminLogOut        *adminLogOutDo
	AdminMenuOut       *adminMenuOutDo
	AdminOrgOut        *adminOrgOutDo
	AdminPolicyOut     *adminPolicyOutDo
	AdminRoleOut       *adminRoleOutDo
	AdminRolePolicy    *adminRolePolicyDo
	AdminUserOut       *adminUserOutDo
}

func (q *Query) WithContext(ctx context.Context) *queryCtx {
	return &queryCtx{
		AdminButtonOut:     q.AdminButtonOut.WithContext(ctx),
		AdminCasbinRuleOut: q.AdminCasbinRuleOut.WithContext(ctx),
		AdminLogOut:        q.AdminLogOut.WithContext(ctx),
		AdminMenuOut:       q.AdminMenuOut.WithContext(ctx),
		AdminOrgOut:        q.AdminOrgOut.WithContext(ctx),
		AdminPolicyOut:     q.AdminPolicyOut.WithContext(ctx),
		AdminRoleOut:       q.AdminRoleOut.WithContext(ctx),
		AdminRolePolicy:    q.AdminRolePolicy.WithContext(ctx),
		AdminUserOut:       q.AdminUserOut.WithContext(ctx),
	}
}

func (q *Query) Transaction(fc func(tx *Query) error, opts ...*sql.TxOptions) error {
	return q.db.Transaction(func(tx *gorm.DB) error { return fc(q.clone(tx)) }, opts...)
}

func (q *Query) Begin(opts ...*sql.TxOptions) *QueryTx {
	return &QueryTx{q.clone(q.db.Begin(opts...))}
}

type QueryTx struct{ *Query }

func (q *QueryTx) Commit() error {
	return q.db.Commit().Error
}

func (q *QueryTx) Rollback() error {
	return q.db.Rollback().Error
}

func (q *QueryTx) SavePoint(name string) error {
	return q.db.SavePoint(name).Error
}

func (q *QueryTx) RollbackTo(name string) error {
	return q.db.RollbackTo(name).Error
}
