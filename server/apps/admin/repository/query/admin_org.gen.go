// Code generated by gorm.io/gen. DO NOT EDIT.
// Code generated by gorm.io/gen. DO NOT EDIT.
// Code generated by gorm.io/gen. DO NOT EDIT.

package query

import (
	"context"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"

	"gorm.io/gen"
	"gorm.io/gen/field"

	"gorm.io/plugin/dbresolver"

	ooutadmin "ia/apps/admin/dto/oout/admin"
	"ia/common/model/admin"
)

func newAdminOrgOut(db *gorm.DB) adminOrgOut {
	_adminOrgOut := adminOrgOut{}

	_adminOrgOut.adminOrgOutDo.UseDB(db)
	_adminOrgOut.adminOrgOutDo.UseModel(&ooutadmin.AdminOrgOut{})

	tableName := _adminOrgOut.adminOrgOutDo.TableName()
	_adminOrgOut.ALL = field.NewField(tableName, "*")
	_adminOrgOut.Id = field.NewInt64(tableName, "id")
	_adminOrgOut.CreatedTime = field.NewString(tableName, "created_time")
	_adminOrgOut.CreatorId = field.NewInt64(tableName, "creator_id")
	_adminOrgOut.UpdatedTime = field.NewString(tableName, "updated_time")
	_adminOrgOut.DeletedTime = field.NewString(tableName, "deleted_time")
	_adminOrgOut.DeleterId = field.NewInt64(tableName, "deleter_id")
	_adminOrgOut.Code = field.NewString(tableName, "code")
	_adminOrgOut.Name = field.NewString(tableName, "name")
	_adminOrgOut.Enable = field.NewInt8(tableName, "enable")
	_adminOrgOut.Area = field.NewString(tableName, "area")
	_adminOrgOut.OfficeAddress = field.NewString(tableName, "office_address")
	_adminOrgOut.RegisterAddress = field.NewString(tableName, "register_address")
	_adminOrgOut.Person = field.NewString(tableName, "person")
	_adminOrgOut.Phone = field.NewString(tableName, "phone")
	_adminOrgOut.Memo = field.NewString(tableName, "memo")
	_adminOrgOut.Creator = adminOrgOutCreator{
		db: db.Session(&gorm.Session{}),

		RelationField: field.NewRelation("Creator", "admin.AdminUser"),
	}

	_adminOrgOut.fillFieldMap()

	return _adminOrgOut
}

type adminOrgOut struct {
	adminOrgOutDo

	ALL             field.Field
	Id              field.Int64
	CreatedTime     field.String
	CreatorId       field.Int64
	UpdatedTime     field.String
	DeletedTime     field.String
	DeleterId       field.Int64
	Code            field.String
	Name            field.String
	Enable          field.Int8
	Area            field.String
	OfficeAddress   field.String
	RegisterAddress field.String
	Person          field.String
	Phone           field.String
	Memo            field.String
	Creator         adminOrgOutCreator

	fieldMap map[string]field.Expr
}

func (a adminOrgOut) Table(newTableName string) *adminOrgOut {
	a.adminOrgOutDo.UseTable(newTableName)
	return a.updateTableName(newTableName)
}

func (a adminOrgOut) As(alias string) *adminOrgOut {
	a.adminOrgOutDo.DO = *(a.adminOrgOutDo.As(alias).(*gen.DO))
	return a.updateTableName(alias)
}

func (a *adminOrgOut) updateTableName(table string) *adminOrgOut {
	a.ALL = field.NewField(table, "*")
	a.Id = field.NewInt64(table, "id")
	a.CreatedTime = field.NewString(table, "created_time")
	a.CreatorId = field.NewInt64(table, "creator_id")
	a.UpdatedTime = field.NewString(table, "updated_time")
	a.DeletedTime = field.NewString(table, "deleted_time")
	a.DeleterId = field.NewInt64(table, "deleter_id")
	a.Code = field.NewString(table, "code")
	a.Name = field.NewString(table, "name")
	a.Enable = field.NewInt8(table, "enable")
	a.Area = field.NewString(table, "area")
	a.OfficeAddress = field.NewString(table, "office_address")
	a.RegisterAddress = field.NewString(table, "register_address")
	a.Person = field.NewString(table, "person")
	a.Phone = field.NewString(table, "phone")
	a.Memo = field.NewString(table, "memo")

	a.fillFieldMap()

	return a
}

func (a *adminOrgOut) GetFieldByName(fieldName string) (field.OrderExpr, bool) {
	_f, ok := a.fieldMap[fieldName]
	if !ok || _f == nil {
		return nil, false
	}
	_oe, ok := _f.(field.OrderExpr)
	return _oe, ok
}

func (a *adminOrgOut) fillFieldMap() {
	a.fieldMap = make(map[string]field.Expr, 16)
	a.fieldMap["id"] = a.Id
	a.fieldMap["created_time"] = a.CreatedTime
	a.fieldMap["creator_id"] = a.CreatorId
	a.fieldMap["updated_time"] = a.UpdatedTime
	a.fieldMap["deleted_time"] = a.DeletedTime
	a.fieldMap["deleter_id"] = a.DeleterId
	a.fieldMap["code"] = a.Code
	a.fieldMap["name"] = a.Name
	a.fieldMap["enable"] = a.Enable
	a.fieldMap["area"] = a.Area
	a.fieldMap["office_address"] = a.OfficeAddress
	a.fieldMap["register_address"] = a.RegisterAddress
	a.fieldMap["person"] = a.Person
	a.fieldMap["phone"] = a.Phone
	a.fieldMap["memo"] = a.Memo

}

func (a adminOrgOut) clone(db *gorm.DB) adminOrgOut {
	a.adminOrgOutDo.ReplaceDB(db)
	return a
}

type adminOrgOutCreator struct {
	db *gorm.DB

	field.RelationField
}

func (a adminOrgOutCreator) Where(conds ...field.Expr) *adminOrgOutCreator {
	if len(conds) == 0 {
		return &a
	}

	exprs := make([]clause.Expression, 0, len(conds))
	for _, cond := range conds {
		exprs = append(exprs, cond.BeCond().(clause.Expression))
	}
	a.db = a.db.Clauses(clause.Where{Exprs: exprs})
	return &a
}

func (a adminOrgOutCreator) WithContext(ctx context.Context) *adminOrgOutCreator {
	a.db = a.db.WithContext(ctx)
	return &a
}

func (a adminOrgOutCreator) Model(m *ooutadmin.AdminOrgOut) *adminOrgOutCreatorTx {
	return &adminOrgOutCreatorTx{a.db.Model(m).Association(a.Name())}
}

type adminOrgOutCreatorTx struct{ tx *gorm.Association }

func (a adminOrgOutCreatorTx) Find() (result *admin.AdminUser, err error) {
	return result, a.tx.Find(&result)
}

func (a adminOrgOutCreatorTx) Append(values ...*admin.AdminUser) (err error) {
	targetValues := make([]interface{}, len(values))
	for i, v := range values {
		targetValues[i] = v
	}
	return a.tx.Append(targetValues...)
}

func (a adminOrgOutCreatorTx) Replace(values ...*admin.AdminUser) (err error) {
	targetValues := make([]interface{}, len(values))
	for i, v := range values {
		targetValues[i] = v
	}
	return a.tx.Replace(targetValues...)
}

func (a adminOrgOutCreatorTx) Delete(values ...*admin.AdminUser) (err error) {
	targetValues := make([]interface{}, len(values))
	for i, v := range values {
		targetValues[i] = v
	}
	return a.tx.Delete(targetValues...)
}

func (a adminOrgOutCreatorTx) Clear() error {
	return a.tx.Clear()
}

func (a adminOrgOutCreatorTx) Count() int64 {
	return a.tx.Count()
}

type adminOrgOutDo struct{ gen.DO }

func (a adminOrgOutDo) Debug() *adminOrgOutDo {
	return a.withDO(a.DO.Debug())
}

func (a adminOrgOutDo) WithContext(ctx context.Context) *adminOrgOutDo {
	return a.withDO(a.DO.WithContext(ctx))
}

func (a adminOrgOutDo) ReadDB() *adminOrgOutDo {
	return a.Clauses(dbresolver.Read)
}

func (a adminOrgOutDo) WriteDB() *adminOrgOutDo {
	return a.Clauses(dbresolver.Write)
}

func (a adminOrgOutDo) Clauses(conds ...clause.Expression) *adminOrgOutDo {
	return a.withDO(a.DO.Clauses(conds...))
}

func (a adminOrgOutDo) Returning(value interface{}, columns ...string) *adminOrgOutDo {
	return a.withDO(a.DO.Returning(value, columns...))
}

func (a adminOrgOutDo) Not(conds ...gen.Condition) *adminOrgOutDo {
	return a.withDO(a.DO.Not(conds...))
}

func (a adminOrgOutDo) Or(conds ...gen.Condition) *adminOrgOutDo {
	return a.withDO(a.DO.Or(conds...))
}

func (a adminOrgOutDo) Select(conds ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.Select(conds...))
}

func (a adminOrgOutDo) Where(conds ...gen.Condition) *adminOrgOutDo {
	return a.withDO(a.DO.Where(conds...))
}

func (a adminOrgOutDo) Exists(subquery interface{ UnderlyingDB() *gorm.DB }) *adminOrgOutDo {
	return a.Where(field.CompareSubQuery(field.ExistsOp, nil, subquery.UnderlyingDB()))
}

func (a adminOrgOutDo) Order(conds ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.Order(conds...))
}

func (a adminOrgOutDo) Distinct(cols ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.Distinct(cols...))
}

func (a adminOrgOutDo) Omit(cols ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.Omit(cols...))
}

func (a adminOrgOutDo) Join(table schema.Tabler, on ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.Join(table, on...))
}

func (a adminOrgOutDo) LeftJoin(table schema.Tabler, on ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.LeftJoin(table, on...))
}

func (a adminOrgOutDo) RightJoin(table schema.Tabler, on ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.RightJoin(table, on...))
}

func (a adminOrgOutDo) Group(cols ...field.Expr) *adminOrgOutDo {
	return a.withDO(a.DO.Group(cols...))
}

func (a adminOrgOutDo) Having(conds ...gen.Condition) *adminOrgOutDo {
	return a.withDO(a.DO.Having(conds...))
}

func (a adminOrgOutDo) Limit(limit int) *adminOrgOutDo {
	return a.withDO(a.DO.Limit(limit))
}

func (a adminOrgOutDo) Offset(offset int) *adminOrgOutDo {
	return a.withDO(a.DO.Offset(offset))
}

func (a adminOrgOutDo) Scopes(funcs ...func(gen.Dao) gen.Dao) *adminOrgOutDo {
	return a.withDO(a.DO.Scopes(funcs...))
}

func (a adminOrgOutDo) Unscoped() *adminOrgOutDo {
	return a.withDO(a.DO.Unscoped())
}

func (a adminOrgOutDo) Create(values ...*ooutadmin.AdminOrgOut) error {
	if len(values) == 0 {
		return nil
	}
	return a.DO.Create(values)
}

func (a adminOrgOutDo) CreateInBatches(values []*ooutadmin.AdminOrgOut, batchSize int) error {
	return a.DO.CreateInBatches(values, batchSize)
}

// Save : !!! underlying implementation is different with GORM
// The method is equivalent to executing the statement: db.Clauses(clause.OnConflict{UpdateAll: true}).Create(values)
func (a adminOrgOutDo) Save(values ...*ooutadmin.AdminOrgOut) error {
	if len(values) == 0 {
		return nil
	}
	return a.DO.Save(values)
}

func (a adminOrgOutDo) First() (*ooutadmin.AdminOrgOut, error) {
	if result, err := a.DO.First(); err != nil {
		return nil, err
	} else {
		return result.(*ooutadmin.AdminOrgOut), nil
	}
}

func (a adminOrgOutDo) Take() (*ooutadmin.AdminOrgOut, error) {
	if result, err := a.DO.Take(); err != nil {
		return nil, err
	} else {
		return result.(*ooutadmin.AdminOrgOut), nil
	}
}

func (a adminOrgOutDo) Last() (*ooutadmin.AdminOrgOut, error) {
	if result, err := a.DO.Last(); err != nil {
		return nil, err
	} else {
		return result.(*ooutadmin.AdminOrgOut), nil
	}
}

func (a adminOrgOutDo) Find() ([]*ooutadmin.AdminOrgOut, error) {
	result, err := a.DO.Find()
	return result.([]*ooutadmin.AdminOrgOut), err
}

func (a adminOrgOutDo) FindInBatch(batchSize int, fc func(tx gen.Dao, batch int) error) (results []*ooutadmin.AdminOrgOut, err error) {
	buf := make([]*ooutadmin.AdminOrgOut, 0, batchSize)
	err = a.DO.FindInBatches(&buf, batchSize, func(tx gen.Dao, batch int) error {
		defer func() { results = append(results, buf...) }()
		return fc(tx, batch)
	})
	return results, err
}

func (a adminOrgOutDo) FindInBatches(result *[]*ooutadmin.AdminOrgOut, batchSize int, fc func(tx gen.Dao, batch int) error) error {
	return a.DO.FindInBatches(result, batchSize, fc)
}

func (a adminOrgOutDo) Attrs(attrs ...field.AssignExpr) *adminOrgOutDo {
	return a.withDO(a.DO.Attrs(attrs...))
}

func (a adminOrgOutDo) Assign(attrs ...field.AssignExpr) *adminOrgOutDo {
	return a.withDO(a.DO.Assign(attrs...))
}

func (a adminOrgOutDo) Joins(fields ...field.RelationField) *adminOrgOutDo {
	for _, _f := range fields {
		a = *a.withDO(a.DO.Joins(_f))
	}
	return &a
}

func (a adminOrgOutDo) Preload(fields ...field.RelationField) *adminOrgOutDo {
	for _, _f := range fields {
		a = *a.withDO(a.DO.Preload(_f))
	}
	return &a
}

func (a adminOrgOutDo) FirstOrInit() (*ooutadmin.AdminOrgOut, error) {
	if result, err := a.DO.FirstOrInit(); err != nil {
		return nil, err
	} else {
		return result.(*ooutadmin.AdminOrgOut), nil
	}
}

func (a adminOrgOutDo) FirstOrCreate() (*ooutadmin.AdminOrgOut, error) {
	if result, err := a.DO.FirstOrCreate(); err != nil {
		return nil, err
	} else {
		return result.(*ooutadmin.AdminOrgOut), nil
	}
}

func (a adminOrgOutDo) FindByPage(offset int, limit int) (result []*ooutadmin.AdminOrgOut, count int64, err error) {
	result, err = a.Offset(offset).Limit(limit).Find()
	if err != nil {
		return
	}

	if size := len(result); 0 < limit && 0 < size && size < limit {
		count = int64(size + offset)
		return
	}

	count, err = a.Offset(-1).Limit(-1).Count()
	return
}

func (a adminOrgOutDo) ScanByPage(result interface{}, offset int, limit int) (count int64, err error) {
	count, err = a.Count()
	if err != nil {
		return
	}

	err = a.Offset(offset).Limit(limit).Scan(result)
	return
}

func (a adminOrgOutDo) Scan(result interface{}) (err error) {
	return a.DO.Scan(result)
}

func (a *adminOrgOutDo) withDO(do gen.Dao) *adminOrgOutDo {
	a.DO = *do.(*gen.DO)
	return a
}
