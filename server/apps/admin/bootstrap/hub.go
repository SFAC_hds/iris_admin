package bootstrap

// import (
// 	"ia/apps/admin/middleware"
// 	"ia/apps/admin/services"
// 	"time"

// 	"github.com/go-playground/validator/v10"
// 	"github.com/kataras/iris/v12"
// 	"github.com/kataras/iris/v12/hero"
// 	"github.com/kataras/iris/v12/middleware/rate"
// )

// // func hub(api iris.Party) {
// func (b *Bootstrapper) hub() {
// 	b.Validator = validator.New()
// 	// 每秒可以1个请求,最大并发6. 每分钟检查,超过5分钟的移除掉
// 	limit := rate.Limit(1, 6, rate.PurgeEvery(time.Minute, 5*time.Minute))
// 	api := b.Party("/api/admin")
// 	api.Use(limit)
// 	api.Use(middleware.ServeHTTP)

// 	api.Party("/user").ConfigureContainer(func(r *iris.APIContainer) {
// 		r.RegisterDependency(services.NewAdminlUserService())

// 		r.Post("/login", Login).Describe("").Title = "用户~login~"
// 		r.Get("/getUserInfo", GetUserInfo).Describe("").Title = "用户~获取用户信息~"
// 		r.Get("/logout", Logout).Describe("").Title = "用户~logout~"
// 		r.Post("/ResetPassword", ResetPassword).Describe("").Title = "用户~重置密码~"
// 		//
// 		r.Post("/", CreateAdminUser).Describe("").Title = "用户~创建~"
// 		r.Delete("/", DeleteAdminUser).Describe("").Title = "用户~删除~"
// 		r.Put("/", EditAdminUser).Describe("").Title = "用户~修改~"
// 		r.Get("/table", TableOfAdminUser).Describe("").Title = "用户~管理报表~"
// 		// r.Get("/list", hero.Handler(impl.List)).Describe("").Title = "用户~列表~"
// 		//
// 		// adminUser.Put("/own/reset_info", hero.Handler(aus.ResetOwnInfo)).Describe("用户管理接口~自己修改自己信息~")
// 		// adminUser.Put("/own/reset_password", hero.Handler(aus.ResetOwnPassword)).Describe("用户管理接口~自己重置密码~")
// 	})

// 	api.Party("/menu").ConfigureContainer(func(r *iris.APIContainer) {
// 		impl := services.NewAdminMenuService()
// 		r.Post("/", hero.Handler(impl.Create)).Describe("").Title = "菜单~创建~"
// 		r.Delete("/", hero.Handler(impl.Delete)).Describe("").Title = "菜单~删除~"
// 		r.Put("/", hero.Handler(impl.Update)).Describe("").Title = "菜单~修改~"
// 		r.Get("/list", hero.Handler(impl.List)).Describe("").Title = "菜单~列表~"
// 		r.Get("/ownList", hero.Handler(impl.OwnList)).Describe("").Title = "菜单~拥有的列表~"
// 	})
// 	api.Party("/button").ConfigureContainer(func(r *iris.APIContainer) {
// 		impl := services.NewAdminButtonService()
// 		r.Post("/", hero.Handler(impl.Create)).Describe("").Title = "按钮~创建~"
// 		r.Post("/generate", impl.Generate).Describe("").Title = "按钮~生成按钮~"
// 		r.Delete("/", hero.Handler(impl.Delete)).Describe("").Title = "按钮~删除~"
// 		r.Put("/", hero.Handler(impl.Update)).Describe("").Title = "按钮~修改~"
// 		// r.Get("/table", hero.Handler(impl.Table)).Describe("").Title = "按钮~管理报表~"
// 		r.Get("/list", hero.Handler(impl.List)).Describe("").Title = "按钮~列表~"
// 	})

// 	api.Party("/role").ConfigureContainer(func(r *iris.APIContainer) {
// 		impl := services.NewAdminRoleService()
// 		r.Post("/", hero.Handler(impl.Create)).Describe("").Title = "角色~创建~"
// 		r.Delete("/", hero.Handler(impl.Delete)).Describe("").Title = "角色~删除~"
// 		r.Put("/", hero.Handler(impl.Update)).Describe("").Title = "角色~修改~"
// 		r.Get("/table", hero.Handler(impl.Table)).Describe("").Title = "角色~管理报表~"
// 		r.Get("/list", hero.Handler(impl.List)).Describe("").Title = "角色~列表~"
// 		r.Post("/grant", hero.Handler(impl.Grant)).Describe("").Title = "角色~授权~"
// 		r.Get("/grant/owned", hero.Handler(impl.Owned)).Describe("").Title = "角色~已有的权限~"
// 	})

// 	api.Party("/policy").ConfigureContainer(func(r *iris.APIContainer) {
// 		impl := services.NewAdminPolicyService()
// 		r.Delete("/", hero.Handler(impl.Delete)).Describe("").Title = "接口策略~删除~"
// 		r.Get("/table", hero.Handler(impl.Table)).Describe("").Title = "接口策略~管理报表~"
// 		r.Get("/reloadPolicy", hero.Handler(impl.ReloadPolicy)).Describe("").Title = "接口策略~重载策略~"
// 		r.Get("/list", hero.Handler(impl.List)).Describe("").Title = "接口策略~列表~"
// 		r.Put("/log", impl.Log).Describe("").Title = "接口策略~日志埋点~"
// 	})

// 	api.Party("/log").ConfigureContainer(func(r *iris.APIContainer) {
// 		impl := services.NewAdminLogService()
// 		r.Delete("/", impl.Delete).Describe("").Title = "操作日志~删除~"
// 		r.Get("/table", impl.Table).Describe("").Title = "操作日志~管理报表~"
// 	})
// }
