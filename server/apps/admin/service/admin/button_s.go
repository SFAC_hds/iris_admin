package admin

import (
	"ia/common/model"
	modeladmin "ia/common/model/admin"
	"ia/common/storage"

	"gorm.io/gorm"
)

type AdminButtonService interface {
	Create(modeladmin.AdminButton) error
	Generate(int64) error
	Edit(modeladmin.AdminButton) error
	DeleteByIdList([]int64) error
	GetList() ([]*modeladmin.AdminButton, error)
}

func NewAdminButtonService() *adminButtonService {
	return &adminButtonService{}
}

type adminButtonService struct{}

func (impl *adminButtonService) Create(in modeladmin.AdminButton) (err error) {
	err = storage.GDB1.Create(&in).Error
	return
}

func (impl *adminButtonService) Generate(uid int64) (err error) {
	var (
		bidList = make([]int64, 0)
		list    = [...]modeladmin.AdminButton{
			{Model: model.Model{CreatorId: uid}, Name: "新建", Place: 0, Enable: 1, Func: "create"},
			{Model: model.Model{CreatorId: uid}, Name: "编辑", Place: 0, Enable: 1, Disable: 1, Func: "edit"},
			{Model: model.Model{CreatorId: uid}, Name: "删除", Place: 0, Enable: 1, Disable: 2, Func: "batchDelete", Tip: 1, TipContent: "警告：数据删除后不可恢复！确定删除吗？"},
			{Model: model.Model{CreatorId: uid}, Name: "复制行", Place: 0, Enable: 1, Disable: 1, Func: "copy", Tip: 1, TipContent: "提示：确定复制该行数据吗？"},
			{Model: model.Model{CreatorId: uid}, Name: "授权", Place: 0, Enable: 1, Disable: 1, Func: "grant"},
			{Model: model.Model{CreatorId: uid}, Name: "重置密码", Place: 0, Enable: 1, Disable: 1, Func: "resetPassword"},
			//
			{Model: model.Model{CreatorId: uid}, Name: "刷新表", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "导入", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "导出", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "打印", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "最大化", Place: 1, Enable: 1},
			{Model: model.Model{CreatorId: uid}, Name: "自定义列", Place: 1, Enable: 1},
		}
	)
	err = storage.GDB1.Transaction(func(tx *gorm.DB) error {
		if err = tx.Model(&modeladmin.AdminButton{}).Select("id").Scan(&bidList).Error; err != nil {
			return err
		}
		if err = tx.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&modeladmin.AdminButton{}).Error; err != nil {
			return err
		}
		if err = tx.Delete(&modeladmin.AdminMenuButton{}, "bid in ?", bidList).Error; err != nil {
			return err
		}
		if err = tx.Model(&modeladmin.AdminRoleMenu{}).Where("bid in ?", bidList).Update("bid", 0).Error; err != nil {
			return err
		}
		//
		if err = tx.CreateInBatches(&list, 100).Error; err != nil {
			return err
		}
		return nil
	})
	return
}

func (impl *adminButtonService) Edit(in modeladmin.AdminButton) (err error) {
	err = storage.GDB1.Save(&in).Error
	return
}

func (impl *adminButtonService) DeleteByIdList(idList []int64) (err error) {
	err = storage.GDB1.Transaction(func(tx *gorm.DB) error {
		if err = tx.Delete(&modeladmin.AdminButton{}, "id in ?", idList).Error; err != nil {
			return err
		}
		if err = tx.Delete(&modeladmin.AdminMenuButton{}, "bid in ?", idList).Error; err != nil {
			return err
		}
		if err = tx.Model(&modeladmin.AdminRoleMenu{}).Where("bid in ?", idList).Update("bid", 0).Error; err != nil {
			return err
		}
		return nil
	})
	return
}

func (impl *adminButtonService) GetList() (rows []*modeladmin.AdminButton, err error) {
	err = storage.GDB1.Find(&rows).Error
	return
}
