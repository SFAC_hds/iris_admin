package admin

import (
	"context"
	oinadmin "ia/apps/admin/dto/oin/admin"
	ooutadmin "ia/apps/admin/dto/oout/admin"
	"ia/apps/admin/repository/query"
	"ia/common/storage"
)

type AdminLogService interface {
	DeleteByIdList([]int64) error
	GetTable(oinadmin.LogIn) ([]*ooutadmin.AdminLogOut, int64, error)
}

func NewAdminLogService() *adminLogService {
	return &adminLogService{}
}

type adminLogService struct{}

func (impl *adminLogService) DeleteByIdList(idList []int64) (err error) {
	do := query.Use(storage.GDB1).AdminLogOut
	_, err = do.Where(do.Id.In(idList...)).Delete()
	return
}

func (impl *adminLogService) GetTable(in oinadmin.LogIn) (rows []*ooutadmin.AdminLogOut, total int64, err error) {
	m := query.Use(storage.GDB1).AdminLogOut
	do := m.WithContext(context.Background())

	if in.UserId > 0 {
		do = do.Where(m.CreatorId.Eq(in.UserId))
	}
	if in.GroupName != "" {
		do = do.Where(m.GroupName.Eq(in.GroupName))
	}
	if in.Name != "" {
		do = do.Where(m.Name.Eq(in.Name))
	}

	rows, total, err = do.FindByPage(in.Offset(), in.PageSize)
	return
}
