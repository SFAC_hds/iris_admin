package admin

import (
	oinadmin "ia/apps/admin/dto/oin/admin"
	ooutadmin "ia/apps/admin/dto/oout/admin"
	modeladmin "ia/common/model/admin"
	"ia/common/storage"

	"gorm.io/gorm"
)

type AdminMenuService interface {
	Create(oinadmin.AdminMenuIn) error
	Edit(oinadmin.AdminMenuIn) error
	DeleteByIdList([]int64) error
	GetList(int64) ([]*ooutadmin.AdminMenuOut, error)
	GetOwnList(int64) ([]*ooutadmin.AdminMenuOut, error)
}

func NewAdminMenuService() *adminMenuService {
	return &adminMenuService{}
}

type adminMenuService struct{}

func (impl *adminMenuService) Create(in oinadmin.AdminMenuIn) (err error) {
	err = storage.GDB1.Transaction(func(tx *gorm.DB) error {
		if err = tx.Create(&in).Error; err != nil {
			return err
		}
		if len(in.BidList) > 0 {
			menuButtons := make([]modeladmin.AdminMenuButton, 0)
			for _, v := range in.BidList {
				menuButtons = append(menuButtons, modeladmin.AdminMenuButton{
					Mid: in.Id,
					Bid: uint(v),
				})
			}
			if err = tx.CreateInBatches(menuButtons, 100).Error; err != nil {
				return err
			}
		}
		return nil
	})
	return
}

func (impl *adminMenuService) Edit(in oinadmin.AdminMenuIn) (err error) {
	err = storage.GDB1.Transaction(func(tx *gorm.DB) error {
		if err = tx.Save(&in).Error; err != nil {
			return err
		}
		if err = tx.Save(&in.Meta).Error; err != nil {
			return err
		}
		if err = tx.Delete(&modeladmin.AdminMenuButton{}, "mid=?", in.Id).Error; err != nil {
			return err
		}
		if len(in.BidList) > 0 {
			menuButtons := make([]modeladmin.AdminMenuButton, 0)
			for _, v := range in.BidList {
				menuButtons = append(menuButtons, modeladmin.AdminMenuButton{
					Mid: in.Id,
					Bid: uint(v),
				})
			}
			if err = tx.CreateInBatches(menuButtons, 100).Error; err != nil {
				return err
			}
		}
		return nil
	})
	return
}

func (impl *adminMenuService) DeleteByIdList(idList []int64) (err error) {
	var metaIdList = make([]int, 0)
	if err = storage.GDB1.Model(&modeladmin.AdminMenu{}).Select("meta_id").
		Where("id in ? or parent_id in ?", idList, idList).Scan(&metaIdList).Error; err != nil {
		return
	}
	if len(metaIdList) > 0 {
		if err = storage.GDB1.Transaction(func(tx *gorm.DB) error {
			if err = tx.Delete(&modeladmin.AdminMenu{}, "id in ? or parent_id in ?", idList, idList).Error; err != nil {
				return err
			}
			if err = tx.Delete(&modeladmin.AdminMenuMeta{}, "id in ?", metaIdList).Error; err != nil {
				return err
			}
			if err = tx.Delete(&modeladmin.AdminRoleMenu{}, "mid in ?", idList).Error; err != nil {
				return err
			}
			if err = tx.Delete(&modeladmin.AdminMenuButton{}, "mid in ?", idList).Error; err != nil {
				return err
			}
			return nil
		}); err != nil {
			return
		}
	}
	return
}

func (impl *adminMenuService) GetList(rid int64) (rows []*ooutadmin.AdminMenuOut, err error) {
	if err = storage.GDB1.Preload("Meta").Preload("Children.Meta").
		Order("sort").
		Find(&rows, "parent_id=0").Error; err != nil {
		return
	}

	if err = impl.deep(rows, rid); err != nil {
		return
	}
	return
}

func (impl *adminMenuService) GetOwnList(uid int64) (rows []*ooutadmin.AdminMenuOut, err error) {
	var (
		midList = make([]int64, 0)
		db      = storage.GDB1.Preload("Meta").Preload("Children.Meta").Order("sort")
	)
	if uid == 1 {
		if err = db.Find(&rows, "parent_id=0").Error; err != nil {
			return
		}
	} else {
		if err = storage.GDB1.Raw(`
		SELECT distinct mid FROM admin_role_menu WHERE rid in (
		SELECT v1 FROM casbin_rule WHERE ptype='g' AND v0=?
		)`, uid).Scan(&midList).Error; err != nil {
			return
		}
		if err = db.Find(&rows, "id in ? AND parent_id=0", midList).Error; err != nil {
			return
		}
	}

	if err = impl.deepOwn(rows, uid, midList); err != nil {
		return
	}
	return
}

//
func (impl *adminMenuService) deep(parent []*ooutadmin.AdminMenuOut, rid int64) error {
	if len(parent) > 0 {
		for _, v1 := range parent {
			// 菜单的所有按钮
			if err := storage.GDB1.Raw(`
			SELECT * FROM admin_button WHERE id in (
			SELECT bid FROM admin_menu_button WHERE mid=?
			) AND enable > 0
			`, v1.Id).Scan(&v1.ButtonList).Error; err != nil {
				return err
			}
			// 获取 角色-菜单 所拥有的按钮集合
			if rid > 0 {
				if err := storage.GDB1.Raw("SELECT bid FROM admin_role_menu WHERE rid=? AND mid=?", rid, v1.Id).
					Scan(&v1.OwnedBidList).Error; err != nil {
					return err
				}
			}

			if len(v1.Children) > 0 {
				if err := storage.GDB1.Preload("Meta").Preload("Children.Meta").
					Order("sort").
					Find(&v1.Children, "parent_id=?", v1.Id).Error; err != nil {
					return err
				}
				if err := impl.deep(v1.Children, rid); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (impl *adminMenuService) deepOwn(parent []*ooutadmin.AdminMenuOut, uid int64, midList []int64) error {
	if len(parent) > 0 {
		for _, v1 := range parent {
			// 角色拥有的按钮
			if uid == 1 {
				if err := storage.GDB1.Raw(`
				SELECT * FROM admin_button WHERE id in (
				SELECT bid FROM admin_menu_button WHERE mid=?
				) AND enable > 0`, v1.Id).Scan(&v1.ButtonList).Error; err != nil {
					return err
				}
			} else {
				if err := storage.GDB1.Raw(`
				SELECT * FROM admin_button WHERE id in (
				SELECT distinct bid FROM admin_role_menu WHERE mid=?) AND enable > 0`, v1.Id).
					Scan(&v1.ButtonList).Error; err != nil {
					return err
				}
			}

			if len(v1.Children) > 0 {
				db := storage.GDB1.Model(&ooutadmin.AdminMenuOut{}).Preload("Meta").Preload("Children.Meta").Order("sort")
				if uid != 1 {
					db.Where("id in ? AND parent_id in ?", midList, midList)
				}
				if err := db.Find(&v1.Children, "parent_id=?", v1.Id).Error; err != nil {
					return err
				}
				if err := impl.deepOwn(v1.Children, uid, midList); err != nil {
					return err
				}
			}
		}
	}
	return nil
}
