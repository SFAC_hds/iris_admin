package main

import (
	"ia/apps/admin/assets/conf"
	"ia/apps/admin/bootstrap"
	"ia/apps/admin/middleware/auth"
	"ia/apps/admin/middleware/logsink"
	"ia/apps/admin/route"
	"ia/common/storage"
	"ia/common/support"
	"ia/common/support/global"
	"ia/common/support/tools"
)

func main() {
	global.InitGlobalConfig(conf.Asset)
	support.InitLog()
	tools.InitGid()
	// storage.InitRedis()
	storage.InitGDb1()
	auth.InitAuth()
	logsink.InitLogSink()

	bt := bootstrap.New("Iris Admin", "614143260@qq.com")
	bt.Bootstrap()
	bt.Configure(route.Hub)
	bt.BuildIfaceList()
	bt.Listen()
}
