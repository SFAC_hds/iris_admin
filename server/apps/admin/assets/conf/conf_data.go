// Code generated for package conf by go-bindata DO NOT EDIT. (@generated)
// sources:
// conf/app.yaml
// conf/db.yaml
package conf

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

// Name return file name
func (fi bindataFileInfo) Name() string {
	return fi.name
}

// Size return file size
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}

// Mode return file mode
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}

// Mode return file modify time
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}

// IsDir return file whether a directory
func (fi bindataFileInfo) IsDir() bool {
	return fi.mode&os.ModeDir != 0
}

// Sys return file is sys mode
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _confAppYaml = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x6c\x92\xdf\x4b\x1b\x4b\x14\xc7\xdf\xf7\xaf\x38\xe8\xab\x71\x37\xb9\xea\x95\x85\x3c\x5c\xa3\x81\x0b\xde\xab\xa0\x72\x1f\x2e\x97\x30\xd9\x9d\x6c\xe6\xba\x3b\xb3\xcc\xcc\x9a\x28\x79\x08\x05\x7f\x15\x41\x6d\x4b\x63\xc1\x17\xa9\xa2\xd8\x96\xfa\x50\x82\x5a\xab\xff\x4c\x66\x8d\x4f\xfe\x0b\x65\x67\x93\x26\x85\x3e\xe5\xfc\xc8\xf9\x9e\xef\x7c\xce\x16\x18\xad\x10\x2f\xe2\x48\x12\x46\x6d\x03\x60\x14\x5c\x22\x50\xd9\xc7\x8d\x0a\x92\xc8\x6f\x60\xce\x19\x6f\xd4\x10\xa7\x0d\x42\x2b\xac\xe1\xe2\x72\xe4\x19\x00\xf3\xcc\x9b\xc7\x6b\xd8\xb7\xa1\x5f\x99\x4d\xe7\x16\x91\xac\x16\x18\xe7\xd8\xd1\x92\x50\x41\xbe\xc0\x06\xc0\x1c\xed\x77\xe7\x84\x83\x42\x3c\xe8\x14\x09\xc7\x7f\x61\x59\x65\xee\xdf\x4c\xfe\xe1\xfb\xac\x86\xdd\x41\xb7\x27\x3b\xc3\xdc\xf5\x02\xa3\x22\x0a\xc2\x44\x77\x81\xae\xd0\x00\x71\x51\x45\xbe\x0d\x92\x47\xc9\x3f\x97\x49\x80\x8b\x8c\x07\x48\xda\x90\xb3\xac\xa9\x8c\x95\xcd\x58\x39\xc8\x4e\xda\xd6\x84\x6d\x4d\x1a\x00\x85\x2a\xe2\x02\x4b\x1b\x56\x96\x8b\x99\xe9\x1f\xa6\x16\x42\x49\x02\xb2\xa1\x19\x88\x9e\x1c\xab\x69\x1c\x02\x87\x88\x23\x39\xe4\x36\x64\x5c\xda\x90\xb5\xac\xe9\xdf\x35\x2e\x2b\xbf\x88\x28\x71\x34\x0c\xc8\xe6\x8b\x09\xb5\x34\xc9\xe5\xe7\x12\x78\x69\xf2\x5b\xfe\x1f\xc4\x69\x1a\x4f\xe4\xff\xa4\x15\x96\xc6\x93\xf9\xd9\x84\x9f\x4e\x0c\x00\x9f\x79\x3c\x12\x25\x3f\x45\x3b\x61\x00\x10\x8f\x32\x8e\x4b\x11\xf7\x85\x0d\xff\x9a\x28\x24\x26\x72\x03\x42\xcd\x48\x60\x6e\xfa\xcc\x23\x74\x0c\x7e\x51\x66\x91\xfc\xa9\xae\xcf\x64\x0a\x89\xa4\x58\x23\x1b\x63\x60\xd6\xc4\x7f\x06\xc0\xff\x35\x59\x92\x24\xc0\x2c\x4a\x5e\x95\xb3\x60\x14\x02\xfd\x6e\x87\x27\xa0\xea\xf5\x7a\x66\x49\xc7\x06\x40\x0d\x97\x05\x73\x56\xb1\x2c\x85\x8c\xf9\x1a\x82\x01\x20\x31\x45\x54\xda\xbd\xdf\x92\xde\x66\x00\xac\x61\x2e\xf4\x07\x90\x1d\xb7\xc6\x2d\x8d\x8a\x04\xc8\xc3\xc2\x86\xa2\xad\x7d\x10\xc7\x4c\x2b\xba\x29\x71\x10\xfa\x9a\xf4\xa0\xdd\xaf\x25\x18\x7a\xb3\x66\x95\x05\xd8\x5c\xaf\x06\x66\x8d\xf1\x55\x11\x22\x07\x9b\x6e\x62\x8a\x67\x1c\x46\x2b\x26\xf5\x08\xad\x9b\x2e\x11\x32\x15\xd7\x06\xfb\xca\x83\xe1\xf8\xed\x76\x7c\xf2\x7e\x78\xc1\x28\xc4\xad\x33\xf5\xd0\x8a\x77\x6f\xe2\x8f\x27\xea\xf4\x5c\x5d\xed\x8f\xd8\x30\xd2\xb9\xbe\xed\xdc\x6e\xa9\xed\x7d\x75\x75\xe8\x96\x1f\xbf\x1d\x3e\x7e\x3d\x56\xf7\xcd\xee\xc3\x9b\xe7\xbb\xbd\x78\xf7\xb5\xda\xdb\x8c\x77\x0e\x3a\xd7\xcd\x78\xf7\x46\x6d\xbd\x53\x9b\x67\xe9\x19\x67\x90\x74\xaa\x4b\x64\x03\xdb\x90\x1b\x92\xef\x6e\x5f\xaa\x97\x17\xf1\xfe\x41\xe7\xf6\xb4\xdb\xde\x8c\x5b\xed\x64\x89\xfa\x74\xa4\x5e\x5c\xf4\x0c\xb4\xda\xcf\x77\x7b\xea\xf8\x22\x3e\xbe\xec\xde\xdf\xab\x9d\xab\xa7\xa3\xa6\x6a\xde\xc5\xad\xf6\xf0\x70\x39\xd1\x4f\x2c\xb4\xda\x4f\xad\x2f\xf1\xe7\x0f\x8f\xe7\xaf\x02\x91\xee\x2e\xb0\x20\x20\x72\xb9\x7f\xd6\x29\xcb\xb2\xbe\x07\x00\x00\xff\xff\x7b\x64\x10\xe5\xe9\x03\x00\x00")

func confAppYamlBytes() ([]byte, error) {
	return bindataRead(
		_confAppYaml,
		"conf/app.yaml",
	)
}

func confAppYaml() (*asset, error) {
	bytes, err := confAppYamlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "conf/app.yaml", size: 1001, mode: os.FileMode(436), modTime: time.Unix(1655349375, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _confDbYaml = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xac\x90\x31\x4b\xfb\x40\x18\x87\xf7\x7c\x8a\x17\xba\x87\x4b\xfa\xff\xd7\xf6\x5d\x9d\x04\xc1\xa1\x1f\x40\x2e\xbd\xd7\x36\x78\xcd\xdb\xde\x5d\xac\x3a\x75\x70\x71\xd1\x41\x8a\x20\x22\x0e\x95\xe2\x56\x1d\x45\xf0\xd3\x24\xd5\x6f\x21\x3d\x13\xba\x08\x0e\x3a\x85\x3c\xcf\x1d\x3f\x9e\x33\xa4\x52\x8b\x01\x80\x54\xca\x20\x68\xee\x49\x3d\x60\xeb\xb0\xd5\xdc\xea\x04\x00\x8d\x4a\x44\x9d\x38\x8c\x5a\xed\xf0\x7f\x18\x8b\xb8\x96\x23\x69\xed\x84\x8d\x5a\xdf\x57\x09\x82\x58\x33\x66\xdd\x4d\x4f\x09\x21\x12\x22\x08\x86\x27\x76\xac\xbf\x7c\xb4\xfe\x00\xa8\x54\x6a\xea\x39\x04\xaf\x3c\xf2\x83\x9b\x6d\xcf\x46\x6c\x1c\x42\xb3\x29\x5a\xfe\x37\xb7\x64\x10\x0c\x73\x65\xeb\xe5\x0d\x52\xd2\xc9\x44\x5a\x42\x48\x4d\x6a\xf7\xa5\x1a\xa6\x99\x17\xbd\x81\x34\x96\x1c\x42\xee\x0e\xda\x9e\xd8\x01\x4f\xba\x63\x8d\xe0\x4c\x4e\x9e\x68\xee\xef\xd2\x11\x69\x04\x45\x49\xde\xf7\x6c\x28\x8f\xf7\x46\x94\x6d\x73\x96\x59\x5f\x23\x6a\xbc\xa3\x34\x55\xb8\x2d\x7c\x5b\xfc\x43\x5b\xfd\x7c\x22\x8c\xc4\xbf\xdf\xf6\x25\xcc\x87\x7f\x5c\x06\x0d\x28\x6f\xa7\xc5\x7c\x51\x9e\x5f\x15\xaf\xd3\xf7\xb7\xbb\xf2\xe2\xa1\x9c\x2d\xbf\x2d\x86\x06\x54\x07\x9e\xee\x57\x37\x67\xab\xc7\x97\x8f\xeb\xe7\x72\xb6\x2c\xe6\x8b\x62\x79\x19\x04\x9f\x01\x00\x00\xff\xff\x8e\xf5\xd9\xec\x54\x02\x00\x00")

func confDbYamlBytes() ([]byte, error) {
	return bindataRead(
		_confDbYaml,
		"conf/db.yaml",
	)
}

func confDbYaml() (*asset, error) {
	bytes, err := confDbYamlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "conf/db.yaml", size: 596, mode: os.FileMode(436), modTime: time.Unix(1654761847, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"conf/app.yaml": confAppYaml,
	"conf/db.yaml":  confDbYaml,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"conf": &bintree{nil, map[string]*bintree{
		"app.yaml": &bintree{confAppYaml, map[string]*bintree{}},
		"db.yaml":  &bintree{confDbYaml, map[string]*bintree{}},
	}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}
