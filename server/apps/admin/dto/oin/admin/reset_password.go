package oinadmin

type ResetPassword struct {
	Id int64 `json:"id"`
	// PasswordOld string `json:"passwordOld"`
	PasswordNew string `json:"passwordNew"`
}
