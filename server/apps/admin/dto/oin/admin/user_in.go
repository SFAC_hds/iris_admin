package oinadmin

import modeladmin "ia/common/model/admin"

type AdminUserIn struct {
	modeladmin.AdminUser
	Roles []int64 `json:"roles" gorm:"-"`
}

func (AdminUserIn) TableName() string {
	return "admin_user"
}

//
type AdminUserLoginForm struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
