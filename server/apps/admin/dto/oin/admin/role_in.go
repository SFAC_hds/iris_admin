package oinadmin

type AdminRoleGrant struct {
	Rid     int64   `json:"rid"`
	PidList []int64 `json:"pidList"`
	MidList []Mid   `json:"midList"`
}

type Mid struct {
	Id  int64   `json:"id"`
	Bid []int64 `json:"bid"`
}
