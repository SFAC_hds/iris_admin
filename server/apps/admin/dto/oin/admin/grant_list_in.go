package oinadmin

import "ia/apps/admin/dto/oin"

type GrantListIn struct {
	oin.VxeTableIn
	UserId int `json:"userId"`
	Status int `json:"status"`
}
