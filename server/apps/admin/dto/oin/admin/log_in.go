package oinadmin

import "ia/apps/admin/dto/oin"

type LogIn struct {
	oin.VxeTableIn
	UserId    int64  `json:"userId"`
	GroupName string `json:"groupName"`
	Name      string `json:"ame"`
}
