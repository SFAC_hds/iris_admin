package oinadmin

import modeladmin "ia/common/model/admin"

type AdminMenuIn struct {
	modeladmin.AdminMenu
	Meta    *modeladmin.AdminMenuMeta `json:"meta" gorm:"foreignKey:MetaId;"`
	BidList []int                     `json:"bidList" gorm:"-"`
}

func (AdminMenuIn) TableName() string {
	return "admin_menu"
}
