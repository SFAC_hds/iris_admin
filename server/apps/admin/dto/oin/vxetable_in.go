package oin

type VxeTableIn struct {
	PageSize    int `json:"pageSize"`
	CurrentPage int `json:"currentPage"`
}

const DefaultPageSize = 50

func (in *VxeTableIn) Offset() (offset int) {
	if in.PageSize <= 0 {
		in.PageSize = DefaultPageSize
	}

	if in.CurrentPage-1 < 0 {
		in.CurrentPage = 1
	}
	offset = (in.CurrentPage - 1) * in.PageSize
	return
}
