package ooutadmin

var GIfaceList []*AdminRoutesOut

// var GIfaceList []any

type AdminRoutesOut struct {
	Path      string `json:"path"`
	Method    string `json:"method"`
	Suf       string `json:"suf"`
	Tenant    string `json:"tenant"`
	GroupName string `json:"groupName"`
	Name      string `json:"name"`
	Memo      string `json:"memo"`
}
