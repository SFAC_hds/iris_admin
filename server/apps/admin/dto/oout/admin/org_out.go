package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminOrgOut struct {
	modeladmin.AdminOrg
	Creator *modeladmin.AdminUser `json:"creator" gorm:"foreignKey:Id;references:CreatorId;"`
}

func (AdminOrgOut) TableName() string {
	return "admin_org"
}
