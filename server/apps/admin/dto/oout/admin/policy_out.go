package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminPolicyOut struct {
	modeladmin.AdminPolicy
}

func (AdminPolicyOut) TableName() string {
	return "admin_policy"
}
