package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminRoleOut struct {
	modeladmin.AdminRole
	Creator *modeladmin.AdminUser `json:"creator" gorm:"foreignKey:Id;"`
}

func (AdminRoleOut) TableName() string {
	return "admin_role"
}
