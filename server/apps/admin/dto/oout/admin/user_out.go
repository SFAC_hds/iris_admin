package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminUserOut struct {
	modeladmin.AdminUser
	Org   *AdminOrgOut    `json:"org" gorm:"foreignKey:OrgId"`
	Roles []*AdminRoleOut `json:"roles" gorm:"-"`
}

func (AdminUserOut) TableName() string {
	return "admin_user"
}
