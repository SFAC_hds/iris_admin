package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminMenuOut struct {
	modeladmin.AdminMenu
	Meta         *modeladmin.AdminMenuMeta `json:"meta" gorm:"foreignKey:MetaId;"`
	Children     []*AdminMenuOut           `json:"children" gorm:"foreignKey:ParentId;"`
	ButtonList   []*AdminButtonOut         `json:"buttonList" gorm:"-"`
	OwnedBidList []int                     `json:"ownedBidList" gorm:"-"`
}

func (AdminMenuOut) TableName() string {
	return "admin_menu"
}
