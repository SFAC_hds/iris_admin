package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminCasbinRuleOut struct {
	modeladmin.CasbinRule
	Creator *modeladmin.AdminUser   `json:"creator" gorm:"foreignKey:Id;"`
	Policy  *modeladmin.AdminPolicy `json:"policy" gorm:"foreignKey:V0;"`
}

func (AdminCasbinRuleOut) TableName() string {
	return "casbin_rule"
}
