package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminLogOut struct {
	modeladmin.AdminLog
	Creator *modeladmin.AdminUser `json:"creator" gorm:"foreignKey:Id;references:CreatorId;"`
}

func (AdminLogOut) TableName() string {
	return "admin_log"
}
