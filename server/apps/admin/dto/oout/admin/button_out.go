package ooutadmin

import modeladmin "ia/common/model/admin"

type AdminButtonOut struct {
	modeladmin.AdminButton
	Creator *modeladmin.AdminUser `json:"creator" gorm:"foreignKey:Id;references:CreatorId;"`
}

func (AdminButtonOut) TableName() string {
	return "admin_button"
}
